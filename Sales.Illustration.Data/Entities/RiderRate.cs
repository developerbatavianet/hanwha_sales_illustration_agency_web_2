﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sales.Illustration.Data.Entities
{
    public partial class RiderRate
    {
        [Key]
        public int RiderRateId { get; set; }
        public Nullable<int> RiderId { get; set; }
        public Nullable<int> RiderTypeId { get; set; }
        public Nullable<int> Age { get; set; }
        public Nullable<double> Rate { get; set; }
        public Nullable<int> RiskClass { get; set; }
        public Nullable<bool> IsActive { get; set; }

        public virtual Rider Rider { get; set; }
        public virtual RiderType RiderType { get; set; }
    }
}
