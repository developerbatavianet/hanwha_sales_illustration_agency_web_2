﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sales.Illustration.Data.Entities
{
    public partial class TopUpWithdrawalData
    {
        [Key]
        public long TopUpWithdrawalDataId { get; set; }
        public string TransCode { get; set; }
        public Nullable<int> TransType { get; set; }
        public Nullable<int> TransYear { get; set; }
        public Nullable<decimal> TransAmount { get; set; }

        public virtual TransLog TransLog { get; set; }
    }
}
