//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Sales.Illustration.Web.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class RiderData
    {
        public long RiderDataId { get; set; }
        public string TransCode { get; set; }
        public string RiderCode { get; set; }
        public Nullable<int> RiderTypeId { get; set; }
        public Nullable<decimal> SumInsured { get; set; }
        public Nullable<decimal> COR { get; set; }
    
        public virtual TransLog TransLog { get; set; }
    }
}
