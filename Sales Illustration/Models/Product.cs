//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Sales.Illustration.Web.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Product
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Product()
        {
            this.ProductAllocationRates = new HashSet<ProductAllocationRate>();
            this.ProductCurrencies = new HashSet<ProductCurrency>();
            this.ProductFunds = new HashSet<ProductFund>();
            this.ProductPaymentMethodPremiums = new HashSet<ProductPaymentMethodPremium>();
            this.ProductRates = new HashSet<ProductRate>();
            this.RiderProducts = new HashSet<RiderProduct>();
        }
    
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public string ProductParent { get; set; }
        public string ProductType { get; set; }
        public Nullable<int> PolicyHolderMinAge { get; set; }
        public Nullable<int> PolicyHolderMaxAge { get; set; }
        public Nullable<int> InsuredMinAge { get; set; }
        public Nullable<int> InsuredMaxAge { get; set; }
        public Nullable<int> ChildMinAge { get; set; }
        public Nullable<int> ChildMaxAge { get; set; }
        public Nullable<int> CovAge { get; set; }
        public Nullable<decimal> AdminFee { get; set; }
        public Nullable<decimal> MinRegularTopUp { get; set; }
        public Nullable<double> RegularTopUpRate { get; set; }
        public Nullable<decimal> MinSingleTopUp { get; set; }
        public Nullable<decimal> MinWithdrawal { get; set; }
        public Nullable<decimal> MinWithdrawalBalance { get; set; }
        public Nullable<int> MinPaymentYear { get; set; }
        public Nullable<int> MinPercSumInsured { get; set; }
        public Nullable<decimal> MinAmountSumInsured { get; set; }
        public Nullable<decimal> MinRemainingBalance { get; set; }
        public Nullable<bool> IsActive { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProductAllocationRate> ProductAllocationRates { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProductCurrency> ProductCurrencies { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProductFund> ProductFunds { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProductPaymentMethodPremium> ProductPaymentMethodPremiums { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProductRate> ProductRates { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RiderProduct> RiderProducts { get; set; }
    }
}
