﻿using log4net;
using Sales.Illustration.Web.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Sales.Illustration.Web.Helper
{
    public class SessionExpire : ActionFilterAttribute
    {
        private static readonly ILog _log = LogManager.GetLogger("");

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            HttpContext ctx = HttpContext.Current;
            // check  sessions here
            if (string.IsNullOrEmpty(_session.AgentCode))
            {
                filterContext.Result = new RedirectResult("~/");
                return;
            }

            base.OnActionExecuting(filterContext);
            Log(filterContext.RouteData);
        }

        private void Log(RouteData routeData)
        {
            var controllerName = routeData.Values["controller"];
            var actionName = routeData.Values["action"];
            var id = routeData.Values["id"];
            _log.Info(_session.AgentCode + " Accessing Pages: " + controllerName + ", Action: " + actionName + ", ID: " + id);
        }
    }

    public class _session : Controller
    {
        //
        // GET: /Session/

        public static string MonthYear
        {
            get
            {
                if (System.Web.HttpContext.Current.Session["MonthYear"] != null)
                    return System.Web.HttpContext.Current.Session["MonthYear"].ToString();
                else return "";
            }
            set { System.Web.HttpContext.Current.Session["MonthYear"] = value; }
        }

        #region Login Data
        public static string AgentCode
        {
            get
            {
                if (System.Web.HttpContext.Current.Session["AgentCode"] != null)
                    return System.Web.HttpContext.Current.Session["AgentCode"].ToString();
                else return "";
            }
            set { System.Web.HttpContext.Current.Session["AgentCode"] = value; }
        }

        public static string AgentName
        {
            get
            {
                if (System.Web.HttpContext.Current.Session["AgentName"] != null)
                    return System.Web.HttpContext.Current.Session["AgentName"].ToString();
                else return "";
            }
            set { System.Web.HttpContext.Current.Session["AgentName"] = value; }
        }

        public static string JoinDate
        {
            get
            {
                if (System.Web.HttpContext.Current.Session["JoinDate"] != null)
                    return System.Web.HttpContext.Current.Session["JoinDate"].ToString();
                else return "";
            }
            set { System.Web.HttpContext.Current.Session["JoinDate"] = value; }
        }

        public static string AAJILicense
        {
            get
            {
                if (System.Web.HttpContext.Current.Session["AAJILicense"] != null)
                    return System.Web.HttpContext.Current.Session["AAJILicense"].ToString();
                else return "";
            }
            set { System.Web.HttpContext.Current.Session["AAJILicense"] = value; }
        }
        public static string Email
        {
            get
            {
                if (System.Web.HttpContext.Current.Session["Email"] != null)
                    return System.Web.HttpContext.Current.Session["Email"].ToString();
                else return "";
            }
            set { System.Web.HttpContext.Current.Session["Email"] = value; }
        }

        public static string RoleUser
        {
            get
            {
                if (System.Web.HttpContext.Current.Session["RoleUser"] != null)
                    return System.Web.HttpContext.Current.Session["RoleUser"].ToString();
                else return "";
            }
            set { System.Web.HttpContext.Current.Session["RoleUser"] = value; }
        }

        public static string AppMode
        {
            get
            {
                if (System.Web.HttpContext.Current.Session["AppMode"] != null)
                    return System.Web.HttpContext.Current.Session["AppMode"].ToString();
                else return "";
            }
            set { System.Web.HttpContext.Current.Session["AppMode"] = value; }
        }

        public static string AppVersion
        {
            get
            {
                if (System.Web.HttpContext.Current.Session["AppVersion"] != null)
                    return System.Web.HttpContext.Current.Session["AppVersion"].ToString();
                else return "";
            }
            set { System.Web.HttpContext.Current.Session["AppVersion"] = value; }
        }

        public static string TransLogCode
        {
            get
            {
                if (System.Web.HttpContext.Current.Session["TransLogCode"] != null)
                    return System.Web.HttpContext.Current.Session["TransLogCode"].ToString();
                else return "";
            }
            set { System.Web.HttpContext.Current.Session["TransLogCode"] = value; }
        }
        #endregion

        public static NasabahDataViewModel dataNasabah
        {
            get
            {
                if (System.Web.HttpContext.Current.Session["dataNasabah"] != null)
                    return System.Web.HttpContext.Current.Session["dataNasabah"] as NasabahDataViewModel;
                else return null;
            }
            set { System.Web.HttpContext.Current.Session["dataNasabah"] = value; }
        }

        public static PremiumAndInvestmentViewModel premiInvestmentData
        {
            get
            {
                if (System.Web.HttpContext.Current.Session["premiInvestmentData"] != null)
                    return System.Web.HttpContext.Current.Session["premiInvestmentData"] as PremiumAndInvestmentViewModel;
                else return null;
            }
            set { System.Web.HttpContext.Current.Session["premiInvestmentData"] = value; }
        }

        public static RiderViewModel riderData
        {
            get
            {
                if (System.Web.HttpContext.Current.Session["riderData"] != null)
                    return System.Web.HttpContext.Current.Session["riderData"] as RiderViewModel;
                else return null;
            }
            set { System.Web.HttpContext.Current.Session["riderData"] = value; }
        }

        public static TopUpWithdrawalViewModel topUpWithData
        {
            get
            {
                if (System.Web.HttpContext.Current.Session["topUpWithData"] != null)
                    return System.Web.HttpContext.Current.Session["topUpWithData"] as TopUpWithdrawalViewModel;
                else return null;
            }
            set { System.Web.HttpContext.Current.Session["topUpWithData"] = value; }
        }

        public static AdditionalInsuredViewModel addInsData
        {
            get
            {
                if (System.Web.HttpContext.Current.Session["addInsData"] != null)
                    return System.Web.HttpContext.Current.Session["addInsData"] as AdditionalInsuredViewModel;
                else return null;
            }
            set { System.Web.HttpContext.Current.Session["addInsData"] = value; }
        }

        public static AdditionalInsuredTraditionalViewModel addInsTraditionalData
        {
            get
            {
                if (System.Web.HttpContext.Current.Session["addInsTraditionalData"] != null)
                    return System.Web.HttpContext.Current.Session["addInsTraditionalData"] as AdditionalInsuredTraditionalViewModel;
                else return null;
            }
            set { System.Web.HttpContext.Current.Session["addInsTraditionalData"] = value; }
        }
        public static InsuranceCostAndRatioViewModel insCostRatioData
        {
            get
            {
                if (System.Web.HttpContext.Current.Session["insCostRatioData"] != null)
                    return System.Web.HttpContext.Current.Session["insCostRatioData"] as InsuranceCostAndRatioViewModel;
                else return null;
            }
            set { System.Web.HttpContext.Current.Session["insCostRatioData"] = value; }
        }

    }
}
