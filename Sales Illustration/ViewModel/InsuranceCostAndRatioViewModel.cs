﻿using System.ComponentModel;
using System.Collections.Generic;

namespace Sales.Illustration.Web.ViewModel
{
    public class InsuranceCostAndRatioViewModel
    {
        public string BiayaAsuransiTotal { get; set; }
        public string RatioPremiBerkala { get; set; }
        public string Premi { get; set; }
        public string BiayaAsuransiTambahan { get; set; }
        public string Language { get; set; }
    }
    public class InsuranceCostTraditionalViewModel
    {
        public string TotalPremi { get; set; }

        public List<ListTertanggung> ListTertanggung { get; set; }
    }

    public class ListTertanggung
    {
        public string NamaTertanggung { get; set; }
        public string Premi { get; set; }
    }  
}