﻿using Sales.Illustration.Web.Domain;
using Sales.Illustration.Web.Helper;
using Sales.Illustration.Web.Models;
using Sales.Illustration.Web.ViewModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace Sales.Illustration.Web.Services
{
    public static class CalculatorServices
    {
        public static CultureInfo GetCulture()
        {
            var culture = "en-US";//(App.Currency == "IDR") ? "id-ID" : "en-US";
            CultureInfo cInfo = new CultureInfo(culture);
            return cInfo;
        }

        public static decimal CurrencyToDecimal(string value)
        {
            if (string.IsNullOrEmpty(value)) return 0;
            value = Regex.Replace(value, @"[^0-9.]", "");
            decimal n;
            decimal.TryParse(value, out n);
            return n;
        }

        public static string DecimalToCurrency(decimal value)
        {
            var cInfo = GetCulture();
            NumberFormatInfo nfi = cInfo.NumberFormat;
            nfi = (NumberFormatInfo)nfi.Clone();
            nfi.CurrencySymbol = "";
            nfi.CurrencyDecimalDigits = 0;
            nfi.CurrencyNegativePattern = 1;
            return String.Format(nfi, "{0:c}", value);
        }

        public static decimal Round(decimal d, int decimals)
        {
            decimal n = (decimal)Math.Pow(10, -decimals);
            return decimal.Round(d / n, 0) * n;
        }

        public static decimal eBaoFactor()
        {
            decimal eBaoFactor = 0.08333333333333M;
            return eBaoFactor;
        }

        public static double MonthlyFundRate(double rate)
        {
            double x = (1 + (rate / 100));
            double y = ((double)1 / (double)12);
            double mPow = Math.Pow(x, y) - 1;
            double mRate = Math.Round(mPow, 4);

            return mRate;
        }

        public static int CalculateAgeOld(DateTime birthDate)
        {
            var today = DateTime.Today;

            if (birthDate <= today)
            {
                var ts = DateTime.Now - birthDate;
                var year = Math.Round(ts.Days / 365.25, 0, MidpointRounding.AwayFromZero);
                var age = (int)year;
                var years = Math.Round(ts.Days / 365.25, 6);
                var x = years - (int)years;

                if (x <= 0.501)
                {
                    age = (int)years;
                }
                else
                {
                    age = (int)Math.Round(years, 0);
                }

                return (int)age;
            }
            else
            {
                return -1;
            }
        }

        public static int CalculateAge(DateTime birthDate)
        {
            var today = DateTime.Today;

            if (birthDate <= today)
            {
                var ts = DateTime.Now - birthDate;
                var month = ((DateTime.Now.Year - birthDate.Year) * 12) + DateTime.Now.Month - birthDate.Month;
                var day = (DateTime.Now.Day - birthDate.Day);

                if (day < 0){
                    month = month - 1;
                    day = 30 + day;
                }

                var age = Math.Round((decimal)(month / 12), 0);
                var sisamonth = month % 12;
                if (sisamonth >= 6 && day >= 0)
                    age = age + 1;

                return (int)age;
            }
            else
            {
                return -1;
            }
        }

        //public static bool CalculateAgeDays(DateTime birthdate)
        //{
        //    var today = DateTime.Today;
        //    TimeSpan span = today.Subtract(birthdate);
        //    var days = (int)span.TotalDays;
        //    if (birthdate <= today && days >= 30)
        //    {
        //        return true;
        //    }
        //    else
        //    {
        //        return false;
        //    }
        //}

        public static int GetInsPeriod(int age,string productCode)
        {
            var period = 40;
            if (productCode == "HLEDU")
            {
                if (period > (age + 27))
                    period = period - age;
                else
                    period = (age + 27) - age;
            }
            else if (productCode == "HLKIDS")
                period = 18 - age;

            return period;
        }

        public static int GetPaymentPeriod(int age, int paymentPeriod, string paymentMode, string productCode)
        {
            var period = 0;
            if (productCode == "HLEDU")
            {
                if (paymentMode == "sekaligus")
                {
                    if (age <= 13)
                        period = 18 - age;
                    else
                        period = 5;
                }
                else
                {
                    if (paymentPeriod == 18)
                        period = 18 - age;
                    else
                    {
                        if ((paymentPeriod + age) <= 18)
                            period = paymentPeriod;
                    }
                }
            } else if(productCode == "HLKIDS")
                period = 18 - age;

            return period;
        }

        public static bool GetRiderSumInsured(int age)
        {
            bool IsCover = false;
            int x = 0;

            if ((99 - age) < (65 - age))
            {
                x = 99 - age;
            }
            else
            {
                x = 65 - age;
            }

            if (x > 0)
            {
                IsCover = true;
            }

            return IsCover;
        }

        public static decimal? CalculateFundWithTopup(decimal? lastYear, decimal? premi, decimal topup, decimal? biayaAdmin, decimal? biayaAsuransi, decimal fundAllocation, decimal fundRate)
        {
            var nilaiInvestasi = (lastYear + premi +
                                topup * fundAllocation -
                                ((biayaAdmin * fundAllocation) / 12) -
                                ((biayaAsuransi * fundAllocation) / 12)) *
                                (1 + fundRate);

            return nilaiInvestasi;
        }

        public static decimal? CalculateFundWithdrawal(decimal? lastYear, decimal? premi, decimal withdrawal, decimal? biayaAdmin, decimal? biayaAsuransi, decimal fundAllocation, decimal fundRate, double? withdrawalChargeRate)
        {
            var nilaiInvestasi = (lastYear + premi -
                                ((biayaAdmin * fundAllocation) / 12) -
                                ((biayaAsuransi * fundAllocation) / 12) - (withdrawal * fundAllocation * ((100 -(decimal)withdrawalChargeRate) / 100))) *
                                (1 + fundRate) -
                                withdrawal * fundAllocation;

            return nilaiInvestasi;
        }

        public static decimal? CalculateFund(decimal? lastYear, decimal? premi, decimal? biayaAdmin, decimal? biayaAsuransi, decimal fundAllocation, decimal fundRate)
        {
            var nilaiInvestasi = (lastYear + premi -
                                ((biayaAdmin * fundAllocation) / 12) -
                                ((biayaAsuransi * fundAllocation) / 12)) *
                                (1 + fundRate);

            return nilaiInvestasi;
        }

        public static decimal? CalculateFundWithSupplementaryAndLoyalty(decimal? lastYear, decimal? premi, decimal? biayaAdmin, decimal? biayaAsuransi, decimal fundAllocation, decimal fundRate, decimal? supplementary,decimal? loyalty)
        {
            var netCash = (lastYear + premi -
                                ((biayaAdmin * fundAllocation) / 12) -
                                ((biayaAsuransi * fundAllocation) / 12));
            var supplementaryFee = Round(Convert.ToDecimal(netCash * supplementary / 100), -1);
            var nilaiInvestasi = (netCash - supplementaryFee) * (1 + fundRate) + (loyalty * fundAllocation);

            return nilaiInvestasi;
        }

        public static decimal? CalculateFundWithTopupSupplementaryAndLoyalty(decimal? lastYear, decimal? premi,decimal topup, decimal? biayaAdmin, decimal? biayaAsuransi, decimal fundAllocation, decimal fundRate, decimal? supplementary, decimal? loyalty)
        {
            var netCash = (lastYear + premi
                                + topup * fundAllocation -
                                ((biayaAdmin * fundAllocation) / 12) -
                                ((biayaAsuransi * fundAllocation) / 12));
            var supplementaryFee = Round(Convert.ToDecimal(netCash * supplementary / 100),-1);
            var nilaiInvestasi = (netCash - supplementaryFee) * (1 + fundRate) + (loyalty * fundAllocation);

            return nilaiInvestasi;
        }

        public static decimal? CalculateFundWithWithdrawalSupplementaryAndLoyalty(decimal? lastYear, decimal? premi, decimal withdrawal, decimal? biayaAdmin, decimal? biayaAsuransi, decimal fundAllocation, decimal fundRate,double? withdrawalChargeRate, decimal? supplementary, decimal? loyalty)
        {
            var netCash = (lastYear + premi -
                                ((biayaAdmin * fundAllocation) / 12) -
                                ((biayaAsuransi * fundAllocation) / 12)) - (withdrawal * fundAllocation * ((100 - (decimal)withdrawalChargeRate) / 100));
            var supplementaryFee = Round(Convert.ToDecimal(netCash * supplementary / 100), -1);
            var nilaiInvestasi = ((netCash - supplementaryFee) * (1 + fundRate) + (loyalty * fundAllocation)) - (withdrawal * fundAllocation);

            return nilaiInvestasi;
        }

        public static decimal CalculateCostOfRider(SummaryViewModel dataInput, decimal up, string riderCode, int? riderType, int age, int? riskClass, string riderCategory, Product product, List<RiderRate> riderRates, List<AnnuityFactor> annuityFactors, int? unit)
        {
            decimal nilaiCOR = 0;
            RiderRate rate;
            AnnuityFactor annFactor;

            if (riderCategory.Equals("Basic"))
            {
                rate = riderRates.FirstOrDefault(x => x.RiderCode == riderCode && x.RiderTypeId == riderType && x.Age == age && x.RiskClass == null && x.Category == riderCategory);

                if (rate != null)
                {
                    nilaiCOR = (decimal)(rate.Rate / 1000) * Convert.ToDecimal(up) * eBaoFactor();
                    nilaiCOR = Round(nilaiCOR, -1);
                    nilaiCOR *= 12;
                }
            }
            else if (riderCategory.Equals("Risk"))
            {
                rate = riderRates.FirstOrDefault(x => x.RiderCode == riderCode && x.RiderTypeId == riderType && x.Age == null && x.RiskClass == riskClass && x.Category == riderCategory);

                if (rate != null)
                {
                    nilaiCOR = (decimal)(rate.Rate / 1000) * Convert.ToDecimal(up) * eBaoFactor();
                    nilaiCOR = Round(nilaiCOR, -1);
                    nilaiCOR *= 12;
                }
            }
            else if (riderCategory.Equals("Choices"))
            {
                var productType = product.ProductCode == "HLWIZ" ? "wiz" : product.ProductType;

                rate = riderRates.FirstOrDefault(x => x.RiderCode == riderCode && x.RiderTypeId == riderType && x.Age == age && x.RiskClass == null && x.Category == riderCategory);
                annFactor = annuityFactors.FirstOrDefault(x => x.Year == age && x.ProductType == productType && x.Factor == 6);

                if (rate != null && annFactor != null)
                {
                    nilaiCOR = (decimal)(rate.Rate / 1000) * Convert.ToDecimal(up) * (decimal)annFactor.Rate * eBaoFactor();
                    nilaiCOR = Round(nilaiCOR, -1);
                    nilaiCOR *= 12;
                }
            }
            else if (riderCategory.Equals("Choice"))
            {
                rate = riderRates.FirstOrDefault(x => x.RiderCode == riderCode && x.RiderTypeId == riderType && x.Age == age && x.RiskClass == null && x.Category == riderCategory);

                if (rate != null)
                {
                    nilaiCOR = (decimal)rate.Rate * eBaoFactor();
                    nilaiCOR = Round(nilaiCOR, -1);
                    nilaiCOR *= 12;
                }
            }
            else if (riderCategory.Equals("Unit"))
            {
                rate = riderRates.FirstOrDefault(x => x.RiderCode == riderCode && x.RiderTypeId == riderType && x.Age == age && x.RiskClass == null && x.Category == riderCategory);

                if (rate != null)
                {
                    nilaiCOR = (decimal)rate.Rate * (decimal)unit * eBaoFactor();
                    nilaiCOR = Round(nilaiCOR, -1);
                    nilaiCOR *= 12;
                }
            }
            else
            {
                nilaiCOR = 0;
            }

            return nilaiCOR;
        }

        public static decimal CalculateCostOfInsurance(string up, double? rate)
        {
            decimal coi = 0;

            coi = (decimal)(rate / 1000) * CurrencyToDecimal(up) * eBaoFactor();
            coi = Round(coi, -1);
            coi = coi * 12;

            return coi;
        }

        public static decimal CalculatePremiLifePro(string up_santunanduka, int age, int mpp, string type, double? factorModeBayar)
        {
            Sales.Illustration.Web.Models.MainModel obj = new Sales.Illustration.Web.Models.MainModel();
            decimal premi = 0;
            var ratePremi = obj.GetTarifPremi(type, age, mpp);

            premi = (decimal)(ratePremi.Tarif / 1000) * CurrencyToDecimal(up_santunanduka) * (decimal)(factorModeBayar / 100) ;
            
            return Round(premi,-1);
        }
        public static Dictionary<string, bool> PaymentMethodMapping()
        {
            Dictionary<string, bool> types = new Dictionary<string, bool>();

            types.Add("11", true); // tahunan

            types.Add("21", true); // semesteran
            types.Add("27", true);

            types.Add("41", true); // kuartalan
            types.Add("44", true);
            types.Add("47", true);
            types.Add("410", true);

            types.Add("121", true); // bulanan
            types.Add("122", true);
            types.Add("123", true);
            types.Add("124", true);
            types.Add("125", true);
            types.Add("126", true);
            types.Add("127", true);
            types.Add("128", true);
            types.Add("129", true);
            types.Add("1210", true);
            types.Add("1211", true);
            types.Add("1212", true);

            return types;
        }

        public static string PaymentModeFrequency(string caraBayar)
        {
            switch (caraBayar)
            {
                case "12": return App_Data.Text.Bulanan;
                case "4": return App_Data.Text.Kuartalan;
                case "2": return App_Data.Text.Semesteran;
                default: return App_Data.Text.Tahunan;
            }
        }
    }
}