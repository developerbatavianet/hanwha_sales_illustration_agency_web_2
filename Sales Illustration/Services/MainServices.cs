﻿using Sales.Illustration.Web.Domain;
using Sales.Illustration.Web.Helper;
using Sales.Illustration.Web.Models;
using Sales.Illustration.Web.ViewModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Sales.Illustration.Web.Services
{
    public static class MainServices
    {
        public static ReportDataOutput GenerateReport(SummaryViewModel dataInput, string agentName, string agentCode, string appVersion)
        {
            string[] errorMsg;
            var errorMsgList = new List<string>();
            var on = new OnlineEntities();
            var off = new OfflineEntities();
            var result = new ReportDataOutput();
            int age;
            DataTable data;
            result.IllustrationDataSet = new DataSet { DataSetName = "ilustrasi" };

            if (dataInput.Nasabah.TertanggungUtama == "Ya")
                age = Convert.ToInt32(dataInput.Nasabah.UmurPemegangPolis);
            else
                age = Convert.ToInt32(dataInput.Nasabah.UmurTertanggungUtama);

            var fundCodeList = dataInput.Premi.Investments.Where(x => x.Percentage > 0).Select(x => x.InvestmentCode).ToList();
            var riderCodeList = dataInput.Rider.Riders.Where(x => x.Checked && CalculatorServices.CurrencyToDecimal(x.BiayaAsuransi) > 0).Select(x => x.Rider.RiderCode).ToList();
            var riderCodeListAddIns = dataInput.Additional.Riders == null ? null : dataInput.Additional.Riders.Where(x => x.Checked || CalculatorServices.CurrencyToDecimal(x.BiayaAsuransi) > 0 || (x.Choices != null ? x.Choices.Where(y => y.Checked).Count() > 0 : true)).Select(x => x.Rider.RiderCode).ToList();

            if (_session.AppMode == "offline")
            {
                var template = on.ReportTemplates.FirstOrDefault(x => x.CultureId == dataInput.Insurance.Language && x.ProductCode == dataInput.Nasabah.NamaProduk);
                var product = on.Products.FirstOrDefault(x => x.ProductCode == dataInput.Nasabah.NamaProduk);
                var riders = on.Riders.Where(x => riderCodeList.Contains(x.RiderCode)).ToList();
                var ridersAddIns = riderCodeListAddIns == null ? null : on.Riders.Where(x => riderCodeListAddIns.Contains(x.RiderCode)).ToList();
                var coiRates = on.RiderRates.Where(x => x.Age >= age && x.Category == null).ToDictionary(x => x.Age, x => x.Rate);
                var productAllocationRates = on.ProductAllocationRates.Where(x => x.ProductCode == dataInput.Nasabah.NamaProduk).ToList();
                var funds = on.Funds.Where(x => fundCodeList.Contains(x.FundCode)).ToList();
                var riderRates = on.RiderRates.Where(x => riderCodeList.Contains(x.RiderCode)).ToList();
                var riderRatesAddIns = riderCodeListAddIns == null ? null : on.RiderRates.Where(x => riderCodeListAddIns.Contains(x.RiderCode)).ToList();
                var productType = product.ProductCode == "HLWIZ" ? "wiz" : product.ProductType;
                var annuityFactors = on.AnnuityFactors.Where(x => x.ProductType == productType).ToList();
                var ridersAll = riders;
                var loyaltyandsupplementary = on.LoyaltySupplementaries.Where(x => x.ProductCode == dataInput.Nasabah.NamaProduk).ToList();

                if (ridersAddIns != null)
                    ridersAll.AddRange(ridersAddIns);

                if (product.ProductCategory.Equals("single"))
                {
                    data = ReportDataServices.CalculateFundBenefitWithRiderAndTopupWithdrawalSingle(dataInput, product, coiRates, productAllocationRates, funds, riderRates, riderRatesAddIns, annuityFactors, out errorMsg);
                    result.IllustrationDataSet.Tables.Add(ReportDataServices.AllocationRateSingle(productAllocationRates, product));
                }
                else
                {
                    data = ReportDataServices.CalculateFundBenefitWithRiderAndTopupWithdrawalRegular(dataInput, product, coiRates, productAllocationRates, funds, riderRates, riderRatesAddIns, annuityFactors, out errorMsg, loyaltyandsupplementary);
                    result.IllustrationDataSet.Tables.Add(ReportDataServices.AllocationRate(productAllocationRates, product, dataInput.Insurance.Language == "en-US"));
                }

                result.IllustrationDataSet.Tables.Add(ReportDataServices.NasabahData(dataInput, product));
                result.IllustrationDataSet.Tables.Add(ReportDataServices.PremiData(dataInput, product));
                result.IllustrationDataSet.Tables.Add(ReportDataServices.RincianAsuransiDasar(dataInput, product));
                result.IllustrationDataSet.Tables.Add(ReportDataServices.RiderDesc(dataInput, riders, ridersAddIns, dataInput.Insurance.Language == "en-US", product));
                result.IllustrationDataSet.Tables.Add(ReportDataServices.RiderWithDesc(dataInput, ridersAll));
                result.IllustrationDataSet.Tables.Add(ReportDataServices.RincianRider(dataInput, riders, product));
                result.IllustrationDataSet.Tables.Add(ReportDataServices.RincianRiderTambahan(dataInput, ridersAddIns, product));
                result.IllustrationDataSet.Tables.Add(ReportDataServices.CreateRingkasanRegular(data, dataInput));
                result.IllustrationDataSet.Tables.Add(ReportDataServices.FundInvestment(dataInput, funds));
                result.IllustrationDataSet.Tables.Add(ReportDataServices.General(dataInput, agentName, agentCode, appVersion, product));
                result.IllustrationDataSet.Tables.Add(ReportDataServices.FundData(dataInput));
                if (product.ProductCategory.Equals("preferred"))
                    result.IllustrationDataSet.Tables.Add(ReportDataServices.WithdrawalRate(productAllocationRates, product,dataInput.Insurance.Language == "en-US"));

                result.ReportName = template.ReportName;
            }
            else
            {
                var template = on.ReportTemplates.FirstOrDefault(x => x.CultureId == dataInput.Insurance.Language && x.ProductCode == dataInput.Nasabah.NamaProduk);
                var product = on.Products.FirstOrDefault(x => x.ProductCode == dataInput.Nasabah.NamaProduk);
                var riders = on.Riders.Where(x => riderCodeList.Contains(x.RiderCode)).ToList();
                var ridersAddIns = riderCodeListAddIns == null ? null : on.Riders.Where(x => riderCodeListAddIns.Contains(x.RiderCode)).ToList();
                var coiRates = on.RiderRates.Where(x => x.Age >= age && x.Category == null).ToDictionary(x => x.Age, x => x.Rate);
                var productAllocationRates = on.ProductAllocationRates.Where(x => x.ProductCode == dataInput.Nasabah.NamaProduk).ToList();
                var funds = on.Funds.Where(x => fundCodeList.Contains(x.FundCode)).ToList();
                var riderRates = on.RiderRates.Where(x => riderCodeList.Contains(x.RiderCode)).ToList();
                var riderRatesAddIns = riderCodeListAddIns == null ? null : on.RiderRates.Where(x => riderCodeListAddIns.Contains(x.RiderCode)).ToList();
                var productType = product.ProductCode == "HLWIZ" ? "wiz" : product.ProductType;
                var annuityFactors = on.AnnuityFactors.Where(x => x.ProductType == productType).ToList();
                var ridersAll = riders;
                var loyaltyandsupplementary = on.LoyaltySupplementaries.Where(x => x.ProductCode == dataInput.Nasabah.NamaProduk).ToList();

                if (ridersAddIns != null)
                    ridersAll.AddRange(ridersAddIns);

                if (product.ProductCategory.Equals("single"))
                {
                    data = ReportDataServices.CalculateFundBenefitWithRiderAndTopupWithdrawalSingle(dataInput, product, coiRates, productAllocationRates, funds, riderRates, riderRatesAddIns, annuityFactors, out errorMsg);
                    result.IllustrationDataSet.Tables.Add(ReportDataServices.AllocationRateSingle(productAllocationRates, product));
                }
                else
                {
                    data = ReportDataServices.CalculateFundBenefitWithRiderAndTopupWithdrawalRegular(dataInput, product, coiRates, productAllocationRates, funds, riderRates, riderRatesAddIns, annuityFactors, out errorMsg, loyaltyandsupplementary);
                    result.IllustrationDataSet.Tables.Add(ReportDataServices.AllocationRate(productAllocationRates, product,dataInput.Insurance.Language == "en-US"));
                }

                result.IllustrationDataSet.Tables.Add(ReportDataServices.NasabahData(dataInput, product));
                result.IllustrationDataSet.Tables.Add(ReportDataServices.PremiData(dataInput, product));
                result.IllustrationDataSet.Tables.Add(ReportDataServices.RincianAsuransiDasar(dataInput, product));
                result.IllustrationDataSet.Tables.Add(ReportDataServices.RiderDesc(dataInput, riders, ridersAddIns, dataInput.Insurance.Language == "en-US", product));
                result.IllustrationDataSet.Tables.Add(ReportDataServices.RiderWithDesc(dataInput, ridersAll));
                result.IllustrationDataSet.Tables.Add(ReportDataServices.RincianRider(dataInput, riders, product));
                result.IllustrationDataSet.Tables.Add(ReportDataServices.RincianRiderTambahan(dataInput, ridersAddIns, product));
                result.IllustrationDataSet.Tables.Add(ReportDataServices.CreateRingkasanRegular(data, dataInput));
                result.IllustrationDataSet.Tables.Add(ReportDataServices.FundInvestment(dataInput, funds));
                result.IllustrationDataSet.Tables.Add(ReportDataServices.General(dataInput, agentName, agentCode, appVersion, product));
                result.IllustrationDataSet.Tables.Add(ReportDataServices.FundData(dataInput));
                if (product.ProductCategory.Equals("preferred"))
                    result.IllustrationDataSet.Tables.Add(ReportDataServices.WithdrawalRate(productAllocationRates, product,dataInput.Insurance.Language == "en-US"));

                result.ReportName = template.ReportName;
            }

            if (dataInput.Premi.Investments == null || result.IllustrationDataSet.Tables["FundData"] == null)
            {
                errorMsgList.Add("Terjadi kesalahan dalam pembuatan ilustrasi.Silahkan diulangi");
            }
            else
            {
                if (dataInput.Premi.Investments.Count <= 0 || result.IllustrationDataSet.Tables["FundData"].Rows.Count <= 0)
                    errorMsgList.Add("Terjadi kesalahan dalam pembuatan ilustrasi.Silahkan diulangi");

                if (dataInput.Premi.Investments.Where(x => x.Percentage > 0).Count() <= 0)
                    errorMsgList.Add("Terjadi kesalahan dalam pembuatan ilustrasi.Silahkan diulangi");

                if (dataInput.Premi.Investments.Sum(x => x.Percentage) < 100)
                    errorMsgList.Add("Terjadi kesalahan dalam pembuatan ilustrasi.Silahkan diulangi");
            }

            result.ErrorMsg = !errorMsgList.Any() ? null : errorMsgList.ToArray();

            return result;
        }

        public static ReportDataOutput GenerateReportTraditional(SummaryViewModel dataInput, string agentName, string agentCode, string appVersion)
        {
            string[] errorMsg;
            var on = new OnlineEntities();
            var off = new OfflineEntities();
            var result = new ReportDataOutput();
            DataTable data;
            List<ProductTahapanRate> tahapansRate = new List<ProductTahapanRate>();
            result.IllustrationDataSet = new DataSet { DataSetName = "ilustrasi" };

            if (dataInput.Rider == null)
            {
                dataInput.Rider = new RiderViewModel();
                dataInput.Rider.BiayaAsuransiTambahan = "0";
            }

            var riderCodeList = dataInput.Rider.Riders != null ? dataInput.Rider.Riders.Where(x => x.Checked && CalculatorServices.CurrencyToDecimal(x.BiayaAsuransi) > 0).Select(x => x.Rider.RiderCode).ToList() :null;

            if (_session.AppMode == "offline")
            {
                var template = on.ReportTemplates.FirstOrDefault(x => x.CultureId == dataInput.Insurance.Language && x.ProductCode == dataInput.Nasabah.NamaProduk);
                var productAllocationRates = on.ProductAllocationRates.Where(x => x.ProductCode == dataInput.Nasabah.NamaProduk).ToList();
                var cashValues = on.CashValueRates.Where(x => x.ProductCode == dataInput.Nasabah.NamaProduk).ToList();
                var tahapanRates = on.ProductTahapanRates.Where(x => x.ProductCode == dataInput.Nasabah.NamaProduk).ToList();
                var product = on.Products.FirstOrDefault(x => x.ProductCode == dataInput.Nasabah.NamaProduk);
                var riders = riderCodeList == null ? null : on.Riders.Where(x => riderCodeList.Contains(x.RiderCode)).ToList();
                var tahapans = on.ProductTahapanRates.Where(x => x.ProductCode == product.ProductCode && (product.ProductCode == "HLKIDS" ? x.ChildAge == dataInput.Nasabah.UmurAnak : true)).ToList();

                if (tahapans != null && product.ProductCode == "HLEDU")
                {
                    var awalTahunTahapan = dataInput.Premi.ModeBayarPremi == "sekaligus" && (5 + dataInput.Nasabah.UmurAnak) > 18 ? (5 + dataInput.Nasabah.UmurAnak) : 18;
                    foreach (var tahapan in tahapans)
                    {
                        var inputTahapan = new ProductTahapanRate();
                        inputTahapan.ProductTahapanRateId = tahapan.ProductTahapanRateId;
                        inputTahapan.ProductCode = tahapan.ProductCode;
                        inputTahapan.Year = tahapan.Year + awalTahunTahapan;
                        inputTahapan.RateTahapan = tahapan.RateTahapan;
                        inputTahapan.IsActive = tahapan.IsActive;
                        tahapansRate.Add(inputTahapan);
                    }
                }
                else
                    tahapansRate = tahapans;

                if (dataInput.Nasabah.NamaProduk == "HLEDU")
                    data = ReportDataServices.CalculateBenefitEducation(dataInput, productAllocationRates, cashValues, tahapansRate, out errorMsg);
                else if(dataInput.Nasabah.NamaProduk == "HLKIDS")
                    data = ReportDataServices.CalculateBenefitKidsPlan(dataInput, productAllocationRates, cashValues, tahapansRate, out errorMsg);
                else
                    data = ReportDataServices.CalculateBenefitLifePro(dataInput, productAllocationRates, cashValues, out errorMsg);

                result.IllustrationDataSet.Tables.Add(ReportDataServices.NasabahData(dataInput, product));
                result.IllustrationDataSet.Tables.Add(ReportDataServices.PremiData(dataInput, product));
                result.IllustrationDataSet.Tables.Add(ReportDataServices.RincianAsuransiDasar(dataInput, product));
                result.IllustrationDataSet.Tables.Add(ReportDataServices.RiderDesc(dataInput, riders, null ,dataInput.Insurance.Language == "en-US", product));
                result.IllustrationDataSet.Tables.Add(ReportDataServices.RiderWithDesc(dataInput, riders));
                result.IllustrationDataSet.Tables.Add(ReportDataServices.RincianRider(dataInput, riders, product));
                result.IllustrationDataSet.Tables.Add(ReportDataServices.DanaTahapan(dataInput,tahapansRate));
                result.IllustrationDataSet.Tables.Add(ReportDataServices.General(dataInput, agentName, agentCode, appVersion, product));
                if (dataInput.Nasabah.NamaProduk == "HLLIFE")
                    result.IllustrationDataSet.Tables.Add(ReportDataServices.DataTertanggungTambahan(dataInput));

                result.ReportName = template.ReportName;
            }
            else
            {
                var template = on.ReportTemplates.FirstOrDefault(x => x.CultureId == dataInput.Insurance.Language && x.ProductCode == dataInput.Nasabah.NamaProduk);
                var productAllocationRates = on.ProductAllocationRates.Where(x => x.ProductCode == dataInput.Nasabah.NamaProduk).ToList();
                var cashValues = on.CashValueRates.Where(x => x.ProductCode == dataInput.Nasabah.NamaProduk).ToList();
                var tahapanRates = on.ProductTahapanRates.Where(x => x.ProductCode == dataInput.Nasabah.NamaProduk).ToList();
                var product = on.Products.FirstOrDefault(x => x.ProductCode == dataInput.Nasabah.NamaProduk);
                var riders = riderCodeList == null ? null : on.Riders.Where(x => riderCodeList.Contains(x.RiderCode)).ToList();
                var tahapans = on.ProductTahapanRates.Where(x => x.ProductCode == product.ProductCode && (product.ProductCode == "HLKIDS" ? x.ChildAge == dataInput.Nasabah.UmurAnak : true)).ToList();

                if (tahapans != null && product.ProductCode == "HLEDU")
                {
                    var awalTahunTahapan = dataInput.Premi.ModeBayarPremi == "sekaligus" && (5 + dataInput.Nasabah.UmurAnak) > 18 ? (5 + dataInput.Nasabah.UmurAnak) : 18;
                    foreach (var tahapan in tahapans)
                    {
                        var inputTahapan = new ProductTahapanRate();
                        inputTahapan.ProductTahapanRateId = tahapan.ProductTahapanRateId;
                        inputTahapan.ProductCode = tahapan.ProductCode;
                        inputTahapan.Year = tahapan.Year + awalTahunTahapan;
                        inputTahapan.RateTahapan = tahapan.RateTahapan;
                        inputTahapan.IsActive = tahapan.IsActive;
                        tahapansRate.Add(inputTahapan);
                    }
                }
                else
                    tahapansRate = tahapans;

                if (dataInput.Nasabah.NamaProduk == "HLEDU")
                    data = ReportDataServices.CalculateBenefitEducation(dataInput, productAllocationRates, cashValues, tahapansRate, out errorMsg);
                else if (dataInput.Nasabah.NamaProduk == "HLKIDS")
                    data = ReportDataServices.CalculateBenefitKidsPlan(dataInput, productAllocationRates, cashValues, tahapansRate, out errorMsg);
                else
                    data = ReportDataServices.CalculateBenefitLifePro(dataInput, productAllocationRates, cashValues, out errorMsg);

                result.IllustrationDataSet.Tables.Add(ReportDataServices.NasabahData(dataInput, product));
                result.IllustrationDataSet.Tables.Add(ReportDataServices.PremiData(dataInput, product));
                result.IllustrationDataSet.Tables.Add(ReportDataServices.RincianAsuransiDasar(dataInput, product));
                result.IllustrationDataSet.Tables.Add(ReportDataServices.RiderDesc(dataInput, riders,null ,dataInput.Insurance.Language == "en-US", product));
                result.IllustrationDataSet.Tables.Add(ReportDataServices.RiderWithDesc(dataInput, riders));
                result.IllustrationDataSet.Tables.Add(ReportDataServices.RincianRider(dataInput, riders, product));
                result.IllustrationDataSet.Tables.Add(ReportDataServices.DanaTahapan(dataInput, tahapansRate));
                result.IllustrationDataSet.Tables.Add(ReportDataServices.General(dataInput, agentName, agentCode, appVersion, product));

                result.ReportName = template.ReportName;
            }

            result.IllustrationDataSet.Tables.Add(ReportDataServices.CreateTraditionalHeader(dataInput));
            result.IllustrationDataSet.Tables.Add(ReportDataServices.CreateRingkasanTraditional(data));
            result.IllustrationDataSet.Tables.Add(ReportDataServices.DataAnakNasabah(dataInput));
            result.IllustrationDataSet.Tables.Add(ReportDataServices.DataTertanggungUtama(dataInput));
            result.IllustrationDataSet.Tables.Add(ReportDataServices.DataInput1(dataInput));
            result.IllustrationDataSet.Tables.Add(ReportDataServices.DataInput2(dataInput));
            if (dataInput.Nasabah.NamaProduk == "HLLIFE")
                result.IllustrationDataSet.Tables.Add(ReportDataServices.DataTertanggungTambahan(dataInput));

            return result;
        }

        public static ReportDataOutput CalculateFundBenefit(SummaryViewModel dataInput)
        {
            string[] errorMsg;
            var result = new ReportDataOutput();
            var on = new OnlineEntities();
            var off = new OfflineEntities();
            var age = 0;
            DataTable data;

            if (dataInput.Nasabah.TertanggungUtama == "Ya")
                age = Convert.ToInt32(dataInput.Nasabah.UmurPemegangPolis);
            else
                age = Convert.ToInt32(dataInput.Nasabah.UmurTertanggungUtama);

            var fundCodeList = dataInput.Premi.Investments.Where(x => x.Percentage > 0).Select(x => x.InvestmentCode).ToList();

            if (_session.AppMode == "offline")
            {
                var product = off.Products.FirstOrDefault(x => x.ProductCode == dataInput.Nasabah.NamaProduk);
                var coiRates = off.RiderRates.Where(x => x.Age >= age && x.Category == null).ToDictionary(x => x.Age, x => x.Rate);
                var productAllocationRates = off.ProductAllocationRates.Where(x => x.ProductCode == dataInput.Nasabah.NamaProduk).ToList();
                var funds = off.Funds.Where(x => fundCodeList.Contains(x.FundCode)).ToList();
                var loyaltyandsupplementary = on.LoyaltySupplementaries.Where(x => x.ProductCode == dataInput.Nasabah.NamaProduk).ToList();

                if (product.ProductCategory.Equals("single"))
                    data = ReportDataServices.CalculateFundBenefitSingle(dataInput, product, coiRates, productAllocationRates, funds, out errorMsg);
                else if (product.ProductCategory.Equals("regular") || product.ProductCategory.Equals("worksite"))
                    data = ReportDataServices.CalculateFundBenefitRegular(dataInput, product, coiRates, productAllocationRates, funds, out errorMsg);
                else if (product.ProductCategory.Equals("preferred"))
                    data = ReportDataServices.CalculateFundBenefitPreffered(dataInput, product, coiRates, productAllocationRates, funds, out errorMsg,loyaltyandsupplementary);
                else //if (product.ProductCategory.Equals("wizer"))
                    data = ReportDataServices.CalculateFundBenefitWizer(dataInput, product, coiRates, productAllocationRates, funds, out errorMsg);
            }
            else
            {
                var product = on.Products.FirstOrDefault(x => x.ProductCode == dataInput.Nasabah.NamaProduk);
                var coiRates = on.RiderRates.Where(x => x.Age >= age && x.Category == null).ToDictionary(x => x.Age, x => x.Rate);
                var productAllocationRates = on.ProductAllocationRates.Where(x => x.ProductCode == dataInput.Nasabah.NamaProduk).ToList();
                var funds = on.Funds.Where(x => fundCodeList.Contains(x.FundCode)).ToList();
                var loyaltyandsupplementary = on.LoyaltySupplementaries.Where(x => x.ProductCode == dataInput.Nasabah.NamaProduk).ToList();

                if (product.ProductCategory.Equals("single"))
                    data = ReportDataServices.CalculateFundBenefitSingle(dataInput, product, coiRates, productAllocationRates, funds, out errorMsg);
                else if (product.ProductCategory.Equals("regular") || product.ProductCategory.Equals("worksite"))
                    data = ReportDataServices.CalculateFundBenefitRegular(dataInput, product, coiRates, productAllocationRates, funds, out errorMsg);
                else if (product.ProductCategory.Equals("preferred"))
                    data = ReportDataServices.CalculateFundBenefitPreffered(dataInput, product, coiRates, productAllocationRates, funds, out errorMsg, loyaltyandsupplementary);
                else //if (product.ProductCategory.Equals("wizer"))
                    data = ReportDataServices.CalculateFundBenefitWizer(dataInput, product, coiRates, productAllocationRates, funds, out errorMsg);
            }

            result.ErrorMsg = errorMsg;

            return result;
        }

        public static ReportDataOutput CalculateFundBenefitWithRider(SummaryViewModel dataInput)
        {
            string[] errorMsg;
            var result = new ReportDataOutput();
            var on = new OnlineEntities();
            var off = new OfflineEntities();
            var age = 0;
            DataTable data;

            if (dataInput.Nasabah.TertanggungUtama == "Ya")
                age = Convert.ToInt32(dataInput.Nasabah.UmurPemegangPolis);
            else
                age = Convert.ToInt32(dataInput.Nasabah.UmurTertanggungUtama);

            var fundCodeList = dataInput.Premi.Investments.Where(x => x.Percentage > 0).Select(x => x.InvestmentCode).ToList();
            var riderCodeList = dataInput.Rider.Riders.Where(x => x.Checked && CalculatorServices.CurrencyToDecimal(x.BiayaAsuransi) > 0).Select(x => x.Rider.RiderCode).ToList();
            var riderCodeListAddIns = dataInput.Additional.Riders == null ? null : dataInput.Additional.Riders.Where(x => x.Checked || CalculatorServices.CurrencyToDecimal(x.BiayaAsuransi) > 0).Select(x => x.Rider.RiderCode).ToList();

            if (_session.AppMode == "offline")
            {
                var product = off.Products.FirstOrDefault(x => x.ProductCode == dataInput.Nasabah.NamaProduk);
                var coiRates = off.RiderRates.Where(x => x.Age >= age && x.Category == null).ToDictionary(x => x.Age, x => x.Rate);
                var productAllocationRates = off.ProductAllocationRates.Where(x => x.ProductCode == dataInput.Nasabah.NamaProduk).ToList();
                var funds = off.Funds.Where(x => fundCodeList.Contains(x.FundCode)).ToList();
                var riderRates = off.RiderRates.Where(x => riderCodeList.Contains(x.RiderCode)).ToList();
                var riderRatesAddIns = riderCodeListAddIns == null ? null : off.RiderRates.Where(x => riderCodeListAddIns.Contains(x.RiderCode)).ToList();
                var productType = product.ProductCode == "HLWIZ" ? "wiz" : product.ProductType;
                var annuityFactors = on.AnnuityFactors.Where(x => x.ProductType == productType).ToList();
                var loyaltyandsupplementary = on.LoyaltySupplementaries.Where(x => x.ProductCode == dataInput.Nasabah.NamaProduk).ToList();

                if (product.ProductCategory.Equals("single"))
                    data = ReportDataServices.CalculateFundBenefitWithRiderSingle(dataInput, product, coiRates, productAllocationRates, funds, riderRates, riderRatesAddIns, annuityFactors, out errorMsg);
                else
                    data = ReportDataServices.CalculateFundBenefitWithRiderRegular(dataInput, product, coiRates, productAllocationRates, funds, riderRates, riderRatesAddIns, annuityFactors, out errorMsg,loyaltyandsupplementary);
            }
            else
            {
                var product = on.Products.FirstOrDefault(x => x.ProductCode == dataInput.Nasabah.NamaProduk);
                var coiRates = on.RiderRates.Where(x => x.Age >= age && x.Category == null).ToDictionary(x => x.Age, x => x.Rate);
                var productAllocationRates = on.ProductAllocationRates.Where(x => x.ProductCode == dataInput.Nasabah.NamaProduk).ToList();
                var funds = on.Funds.Where(x => fundCodeList.Contains(x.FundCode)).ToList();
                var riderRates = on.RiderRates.Where(x => riderCodeList.Contains(x.RiderCode)).ToList();
                var riderRatesAddIns = riderCodeListAddIns == null ? null : on.RiderRates.Where(x => riderCodeListAddIns.Contains(x.RiderCode)).ToList();
                var productType = product.ProductCode == "HLWIZ" ? "wiz" : product.ProductType;
                var annuityFactors = on.AnnuityFactors.Where(x => x.ProductType == productType).ToList();
                var loyaltyandsupplementary = on.LoyaltySupplementaries.Where(x => x.ProductCode == dataInput.Nasabah.NamaProduk).ToList();

                if (product.ProductCategory.Equals("single"))
                    data = ReportDataServices.CalculateFundBenefitWithRiderSingle(dataInput, product, coiRates, productAllocationRates, funds, riderRates, riderRatesAddIns, annuityFactors, out errorMsg);
                else
                    data = ReportDataServices.CalculateFundBenefitWithRiderRegular(dataInput, product, coiRates, productAllocationRates, funds, riderRates, riderRatesAddIns, annuityFactors, out errorMsg, loyaltyandsupplementary);
            }

            result.ErrorMsg = errorMsg;

            return result;
        }

        public static ReportDataOutput CalculateFundBenefitTopupWithdrawal(SummaryViewModel dataInput)
        {
            string[] errorMsg;
            var result = new ReportDataOutput();
            var on = new OnlineEntities();
            var off = new OfflineEntities();
            var age = 0;
            DataTable data;

            if (dataInput.Nasabah.TertanggungUtama == "Ya")
                age = Convert.ToInt32(dataInput.Nasabah.UmurPemegangPolis);
            else
                age = Convert.ToInt32(dataInput.Nasabah.UmurTertanggungUtama);

            var fundCodeList = dataInput.Premi.Investments.Where(x => x.Percentage > 0).Select(x => x.InvestmentCode).ToList();
            var riderCodeList = dataInput.Rider.Riders.Where(x => x.Checked && CalculatorServices.CurrencyToDecimal(x.BiayaAsuransi) > 0).Select(x => x.Rider.RiderCode).ToList();
            var riderCodeListAddIns = dataInput.Additional.Riders == null ? null : dataInput.Additional.Riders.Where(x => x.Checked || CalculatorServices.CurrencyToDecimal(x.BiayaAsuransi) > 0).Select(x => x.Rider.RiderCode).ToList();

            if (_session.AppMode == "offline")
            {
                var product = off.Products.FirstOrDefault(x => x.ProductCode == dataInput.Nasabah.NamaProduk);
                var coiRates = off.RiderRates.Where(x => x.Age >= age && x.Category == null).ToDictionary(x => x.Age, x => x.Rate);
                var productAllocationRates = off.ProductAllocationRates.Where(x => x.ProductCode == dataInput.Nasabah.NamaProduk).ToList();
                var funds = off.Funds.Where(x => fundCodeList.Contains(x.FundCode)).ToList();
                var riderRates = off.RiderRates.Where(x => riderCodeList.Contains(x.RiderCode)).ToList();
                var riderRatesAddIns = riderCodeListAddIns == null ? null : off.RiderRates.Where(x => riderCodeListAddIns.Contains(x.RiderCode)).ToList();
                var productType = product.ProductCode == "HLWIZ" ? "wiz" : product.ProductType;
                var annuityFactors = on.AnnuityFactors.Where(x => x.ProductType == productType).ToList();
                var loyaltyandsupplementary = on.LoyaltySupplementaries.Where(x => x.ProductCode == dataInput.Nasabah.NamaProduk).ToList();

                if (product.ProductCategory.Equals("single"))
                    data = ReportDataServices.CalculateFundBenefitWithRiderAndTopupWithdrawalSingle(dataInput, product, coiRates, productAllocationRates, funds, riderRates, riderRatesAddIns, annuityFactors, out errorMsg);
                else
                    data = ReportDataServices.CalculateFundBenefitWithRiderAndTopupWithdrawalRegular(dataInput, product, coiRates, productAllocationRates, funds, riderRates, riderRatesAddIns, annuityFactors, out errorMsg,loyaltyandsupplementary);
            }
            else
            {
                var product = on.Products.FirstOrDefault(x => x.ProductCode == dataInput.Nasabah.NamaProduk);
                var coiRates = on.RiderRates.Where(x => x.Age >= age && x.Category == null).ToDictionary(x => x.Age, x => x.Rate);
                var productAllocationRates = on.ProductAllocationRates.Where(x => x.ProductCode == dataInput.Nasabah.NamaProduk).ToList();
                var funds = on.Funds.Where(x => fundCodeList.Contains(x.FundCode)).ToList();
                var riderRates = on.RiderRates.Where(x => riderCodeList.Contains(x.RiderCode)).ToList();
                var riderRatesAddIns = riderCodeListAddIns == null ? null : on.RiderRates.Where(x => riderCodeListAddIns.Contains(x.RiderCode)).ToList();
                var productType = product.ProductCode == "HLWIZ" ? "wiz" : product.ProductType;
                var annuityFactors = on.AnnuityFactors.Where(x => x.ProductType == productType).ToList();
                var loyaltyandsupplementary = on.LoyaltySupplementaries.Where(x => x.ProductCode == dataInput.Nasabah.NamaProduk).ToList();

                if (product.ProductCategory.Equals("single"))
                    data = ReportDataServices.CalculateFundBenefitWithRiderAndTopupWithdrawalSingle(dataInput, product, coiRates, productAllocationRates, funds, riderRates, riderRatesAddIns, annuityFactors, out errorMsg);
                else
                    data = ReportDataServices.CalculateFundBenefitWithRiderAndTopupWithdrawalRegular(dataInput, product, coiRates, productAllocationRates, funds, riderRates, riderRatesAddIns, annuityFactors, out errorMsg,loyaltyandsupplementary);
            }

            result.ErrorMsg = errorMsg;

            return result;
        }

        public static TransLogOutput CreateTransLog(SummaryViewModel dataInput, string agentCode)
        {
            string transCode;
            var result = new TransLogOutput();
            result.TransCode = TransLogServices.CreateTransLog(dataInput, agentCode, out transCode);

            return result;
        }

        public static void UpdateTransLog(SummaryViewModel dataInput, string agentCode, string transCode)
        {   
            TransLogServices.UpdateTransLog(dataInput, agentCode, transCode);
        }

        public static decimal CalculateCostOfRider(SummaryViewModel dataInput, decimal up, string riderCode, int? riderType, int age, int? riskClass, string riderCategory, Product product, int? unit)
        {
            var on = new OnlineEntities();
            var off = new OfflineEntities();
            decimal cor = 0;
            var productType = product.ProductCode == "HLWIZ" ? "wiz" : product.ProductType;

            if (_session.AppMode == "offline")
            {
                var riderRates = on.RiderRates.Where(x => x.RiderCode == riderCode).ToList();
                var annuityFactors = on.AnnuityFactors.Where(x => x.ProductType == productType).ToList();

                cor = CalculatorServices.CalculateCostOfRider(dataInput, up, riderCode, riderType, age, riskClass, riderCategory, product, riderRates, annuityFactors, unit);
            }
            else
            {
                var riderRates = on.RiderRates.Where(x => x.RiderCode == riderCode).ToList();
                var annuityFactors = on.AnnuityFactors.Where(x => x.ProductType == productType).ToList();

                cor = CalculatorServices.CalculateCostOfRider(dataInput, up, riderCode, riderType, age, riskClass, riderCategory, product, riderRates, annuityFactors, unit);
            }

            return cor;
        }

        public static decimal CalculateCostOfInsurance(SummaryViewModel dataInput)
        {
            var on = new OnlineEntities();
            var off = new OfflineEntities();
            decimal coi = 0;
            var age = 0;

            if (dataInput.Nasabah.TertanggungUtama == "Ya")
                age = Convert.ToInt32(dataInput.Nasabah.UmurPemegangPolis);
            else
                age = Convert.ToInt32(dataInput.Nasabah.UmurTertanggungUtama);

            if (_session.AppMode == "offline")
            {
                var upRates = on.RiderRates.FirstOrDefault(x => x.Age == age && x.RiderCode == null && x.Category == null);
                coi = CalculatorServices.CalculateCostOfInsurance(dataInput.Premi.UangPertanggungan, upRates.Rate);
            }
            else
            {
                var upRates = on.RiderRates.FirstOrDefault(x => x.Age == age && x.RiderCode == null && x.Category == null);
                coi = CalculatorServices.CalculateCostOfInsurance(dataInput.Premi.UangPertanggungan, upRates.Rate);
            }

            return coi;
        }

        public static decimal CalculateTraditionalCOR(string up, string code, int? ridertype, string type, int? riderCov, string category)
        {
            Sales.Illustration.Web.Models.MainModel obj = new Sales.Illustration.Web.Models.MainModel();
            if (category == "Basic")
            {
                int basicCov = Convert.ToInt32(_session.premiInvestmentData.MasaAsuransi);
                int insAge = 0;
                int basicTerm = 1;
                int riderTerm = 1;

                if (type != "sekaligus")
                {
                    basicTerm = Convert.ToInt32(_session.premiInvestmentData.RencanaMasaPembayaran);
                    riderTerm = Convert.ToInt32(_session.premiInvestmentData.RencanaMasaPembayaran);
                }

                if (_session.dataNasabah.TertanggungUtama == "Ya")
                    insAge = Convert.ToInt32(_session.dataNasabah.UmurPemegangPolis);
                else
                    insAge = Convert.ToInt32(_session.dataNasabah.UmurTertanggungUtama);

                var rate = obj.GetTraditionalRiderRate(code, null, type, insAge, basicCov, basicTerm, riderCov, riderTerm);
                var cor = (rate.Rate / 1000) * Convert.ToDouble(up);
                return CalculatorServices.Round((decimal)cor, 0);
            }
            else //if (category == "Choices")
            {
                int insAge = 0;
                var product = obj.GetProduct(_session.dataNasabah.NamaProduk);
                int basicTerm = Convert.ToInt32(_session.premiInvestmentData.RencanaMasaPembayaran);

                if (_session.dataNasabah.TertanggungUtama == "Ya")
                    insAge = Convert.ToInt32(_session.dataNasabah.UmurPemegangPolis);
                else
                    insAge = Convert.ToInt32(_session.dataNasabah.UmurTertanggungUtama);

                var rate = obj.GetTraditionalRiderRate(code, ridertype, type, insAge, basicTerm, basicTerm, null, null);
                var premi = Convert.ToDouble(CalculatorServices.CurrencyToDecimal(_session.premiInvestmentData.PremiBerkala));
                var factor = Convert.ToDouble(_session.premiInvestmentData.CaraBayar);
                var annFactor = obj.GetAnnuityFactor(basicTerm, null, product.ProductType);
                var upR = premi * factor;
                var cor = (rate.Rate / 1000) * upR * annFactor.Rate;
                return  CalculatorServices.Round((decimal)cor,0);
            }
        }

        public static decimal CalculateUnitLinkCOR(decimal up, string riderCode, int? riderType, int age, int? riskClass, string riderCategory, int? unit)
        {
            Sales.Illustration.Web.Models.MainModel obj = new Sales.Illustration.Web.Models.MainModel();
            SummaryViewModel dataInput = new SummaryViewModel();
            var product = obj.GetProduct(_session.dataNasabah.NamaProduk);
            if (riderCategory.Equals("Choices"))
            {
                up = CalculatorServices.CurrencyToDecimal(_session.premiInvestmentData.PremiBerkala) + CalculatorServices.CurrencyToDecimal(_session.premiInvestmentData.TopupBerkala);

                var cor = MainServices.CalculateCostOfRider(dataInput, up, riderCode, riderType, age, riskClass, riderCategory, product, unit);

                return CalculatorServices.Round((decimal)cor, 0);
            }
            else if (riderCategory.Equals("Unit"))
            {
                var cor = MainServices.CalculateCostOfRider(dataInput, up, riderCode, riderType, age, riskClass, riderCategory, product, unit);

                return CalculatorServices.Round((decimal)cor, 0);
            }
            else
            {
                var cor = MainServices.CalculateCostOfRider(dataInput, up, riderCode, riderType, age, riskClass, riderCategory, product, unit);

                return CalculatorServices.Round((decimal)cor, 0);
            }
        }

        public static InsuranceCostTraditionalViewModel CalculatePremiAllTertanggung(string santunanduka, string up, int phAge, int? insuredAge,double? paymentMethod,int mpp, bool isTTu)
        {
            var ret = new InsuranceCostTraditionalViewModel();
            List<ListTertanggung> ListTertanggung = new List<ListTertanggung>();
            decimal premi;
            var umurTertanggung = _session.dataNasabah.TertanggungUtama == "Ya" ? _session.dataNasabah.UmurPemegangPolis : _session.dataNasabah.UmurTertanggungUtama.Value;
            decimal TotalPremi = 0;

            premi = CalculatorServices.CalculatePremiLifePro(santunanduka, _session.dataNasabah.UmurPemegangPolis, _session.premiInvestmentData.RencanaMasaPembayaran, "PA", paymentMethod);
            TotalPremi += premi;
            ListTertanggung.Add(new ListTertanggung { NamaTertanggung = "Pemegang Polis", Premi = CalculatorServices.DecimalToCurrency(premi) });

            premi = CalculatorServices.CalculatePremiLifePro(_session.premiInvestmentData.UangPertanggungan, umurTertanggung, _session.premiInvestmentData.RencanaMasaPembayaran, "NON_PA", paymentMethod);
            TotalPremi += premi;
            ListTertanggung.Add(new ListTertanggung { NamaTertanggung = "Tertanggung Utama", Premi = CalculatorServices.DecimalToCurrency(premi) });

            var no = 1;
            foreach (var ttgTambahan in _session.addInsTraditionalData.TertanggungTambahan)
            {
                premi = CalculatorServices.CalculatePremiLifePro(santunanduka, ttgTambahan.Age.Value, _session.premiInvestmentData.RencanaMasaPembayaran, "PA", paymentMethod);
                TotalPremi += premi;
                ListTertanggung.Add(new ListTertanggung { NamaTertanggung = "Tertanggung Tambahan " + no, Premi = CalculatorServices.DecimalToCurrency(premi) });
                no = no + 1;
            }
            ret.ListTertanggung = ListTertanggung;
            ret.TotalPremi = CalculatorServices.DecimalToCurrency(TotalPremi);

            return ret;
        }
        public static double? GetFactorPaymentMethod(int code)
        {
            Sales.Illustration.Web.Models.MainModel obj = new Sales.Illustration.Web.Models.MainModel();

            var factor = obj.GetPaymentMethod(code);

            return factor.Factor;
        }

        public static string GetRelationshipText(string code)
        {
            var ret = "";

            if (code == "Pasangan")
                ret = App_Data.Text.Pasangan;
            else if (code == "Anak Angkat")
                ret = App_Data.Text.AnakAngkat;
            else if (code == "Anak Kandung")
                ret = App_Data.Text.AnakKandung;
            else if (code == "Orang Tua Angkat")
                ret = App_Data.Text.OrangTuaAngkat;
            else if (code == "Orang Tua Kandung")
                ret = App_Data.Text.OrangTuaKandung;
            else if (code == "" || code == null)
                ret = App_Data.Text.DiriSendiri;
            else
                ret = code;

            return ret;
        }
    }
}