﻿using Sales.Illustration.Web.Extensions;
using Sales.Illustration.Web.Helper;
using Sales.Illustration.Web.Models;
using Sales.Illustration.Web.Services;
using Sales.Illustration.Web.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace Sales.Illustration.Web.Controllers
{
    public class AjaxController : Controller
    {
        //
        // GET: /Ajax/
        Sales.Illustration.Web.Models.MainModel obj = new Sales.Illustration.Web.Models.MainModel();

        #region General
        [HttpPost]
        public virtual JsonResult GetRiskClass(int id)
        {
            return Json(obj.GetRiskClass(id));
        }

        [HttpPost]
        public virtual JsonResult CalculateAge(string dob)
        {
            var birthDate = DateTime.ParseExact(dob, "dd/MM/yyyy", null);

            return Json(CalculatorServices.CalculateAge(birthDate));
        }

        [HttpPost]
        public virtual JsonResult GetProduct(string code)
        {
            var data = obj.GetProduct(code);

            return Json(new
            {
                ProductCode = data.ProductCode,
                ProductName = data.ProductName,
                ProductParent = data.ProductParent,
                ProductType = data.ProductType,
                PolicyHolderMinAge = data.PolicyHolderMinAge,
                PolicyHolderMaxAge = data.PolicyHolderMaxAge,
                InsuredMinAge = data.InsuredMinAge,
                InsuredMaxAge = data.InsuredMaxAge,
                ChildMinAge = data.ChildMinAge,
                ChildMaxAge = data.ChildMaxAge,
                CovAge = data.CovAge,
                AdminFee = data.AdminFee,
                MinRegularTopUp = data.MinRegularTopUp,
                RegularTopUpRate = data.RegularTopUpRate,
                MinSingleTopUp = data.MinSingleTopUp,
                MinWithdrawal = data.MinWithdrawal,
                MinWithdrawalBalance = data.MinWithdrawalBalance,
                MinPaymentYear = data.MinPaymentYear,
                MinPercSumInsured = data.MinPercSumInsured,
                MinAmountSumInsured = data.MinAmountSumInsured,
                MinRemainingBalance = data.MinRemainingBalance,
                MinBasicPremium = data.MinBasicPremium
            });
        }

        [HttpPost]
        public virtual JsonResult GetProductCode()
        {
            return Json(_session.dataNasabah.NamaProduk);
        }

        [HttpPost]
        public virtual JsonResult GetMinAmountByPaymentMethod(string product, int pm, string type)
        {
            var data = obj.GetProductPaymentMethodPremium(product, pm, type);

            return Json(new
            {
                MinAmount = data.MinAmount
            });
        }

        [HttpPost]
        public virtual JsonResult GetPaymentMethod(int code)
        {
            var data = obj.GetPaymentMethod(code);
            return Json(new
            {
                PMCode = data.PMCode,
                PMName = data.PMName,
                Factor = data.Factor
            });
        }

        [HttpPost]
        public virtual JsonResult GetRiderSumInsured(int age)
        {
            return Json(CalculatorServices.GetRiderSumInsured(age));
        }

        [HttpPost]
        public virtual JsonResult GetUnitMapping(string riderCode, int? riderType, decimal up)
        {
            var unitMap = obj.GetUnitMapping(riderCode, riderType, up);

            if (unitMap != null)
                return Json(unitMap);
            else
                return Json("0");
        }

        [HttpPost]
        public virtual JsonResult GetMaxUnitMapping(string riderCode, int? riderType, decimal up)
        {
            var unitMap = obj.GetMaxUnitMapping(riderCode, riderType, up);

            if (unitMap != null)
                return Json(unitMap);
            else
                return Json("0");
        }

        [HttpPost]
        public virtual JsonResult GetSantunanHarian(string riderCode, int riderType)
        {
            var santunan = obj.GetSantunanHarian(riderCode, riderType);

            if (santunan != null)
                return Json(santunan.Santunan);
            else
                return Json("0");
        }

        [HttpPost]
        public virtual JsonResult IsCoveredByRider(string RiderCode, int age)
        {
            var rider = obj.GetActiveRider().Where(x => x.RiderCode == RiderCode).First();
            var maxAge = rider.InsuredMaxAge;
            var minAge = rider.InsuredMinAge;

            var covered = age >= minAge / 12 && age <= maxAge / 12;

            return Json(covered);
        }
        #endregion

        #region Traditional
        [HttpPost]
        public virtual JsonResult GetInsPeriod(int age, string productCode)
        {
            return Json(CalculatorServices.GetInsPeriod(age, productCode));
        }

        [HttpPost]
        public virtual JsonResult GetPaymentPeriod(int age, int paymentPeriod, string paymentMode, string productCode)
        {
            return Json(CalculatorServices.GetPaymentPeriod(age, paymentPeriod, paymentMode,productCode));
        }

        [HttpPost]
        public virtual JsonResult CalculatePremiTraditional(string up, string type, int childAge, int term, int period, string productCode)
        {
            int insAge = 0; double? rate;

            if (_session.dataNasabah.TertanggungUtama == "Ya")
                insAge = Convert.ToInt32(_session.dataNasabah.UmurPemegangPolis);
            else
                insAge = Convert.ToInt32(_session.dataNasabah.UmurTertanggungUtama);

            if (productCode == "HLEDU")
                rate = obj.GetTraditionalRate(type, childAge, insAge, term, period).Rate;
            else
                rate = obj.GetProductRate(productCode, childAge, insAge).Rate;

            var coi = (rate / 1000) * Convert.ToDouble(up);
            return Json(coi);
        }

        [HttpPost]
        public virtual JsonResult CalculateTraditionalCOR(string up, string code, int? ridertype, string type, int? riderCov, string category)
        {
            if (category == "Basic")
            {
                int basicCov = Convert.ToInt32(_session.premiInvestmentData.MasaAsuransi);
                int insAge = 0;
                int basicTerm = 1;
                int riderTerm = 1;

                if (type != "sekaligus")
                {
                    basicTerm = Convert.ToInt32(_session.premiInvestmentData.RencanaMasaPembayaran);
                    riderTerm = Convert.ToInt32(_session.premiInvestmentData.RencanaMasaPembayaran);
                }

                if (_session.dataNasabah.TertanggungUtama == "Ya")
                    insAge = Convert.ToInt32(_session.dataNasabah.UmurPemegangPolis);
                else
                    insAge = Convert.ToInt32(_session.dataNasabah.UmurTertanggungUtama);

                var rate = obj.GetTraditionalRiderRate(code, null, type, insAge, basicCov, basicTerm, riderCov, riderTerm);
                var cor = (rate.Rate / 1000) * Convert.ToDouble(up);
                return Json(cor);
            }
            else //if (category == "Choices")
            {
                int insAge = 0;
                var product = obj.GetProduct(_session.dataNasabah.NamaProduk);
                int basicTerm = Convert.ToInt32(_session.premiInvestmentData.RencanaMasaPembayaran);

                if (_session.dataNasabah.TertanggungUtama == "Ya")
                    insAge = Convert.ToInt32(_session.dataNasabah.UmurPemegangPolis);
                else
                    insAge = Convert.ToInt32(_session.dataNasabah.UmurTertanggungUtama);

                var rate = obj.GetTraditionalRiderRate(code, ridertype, type, insAge, basicTerm, basicTerm, null, null);
                var premi = Convert.ToDouble(CalculatorServices.CurrencyToDecimal(_session.premiInvestmentData.PremiBerkala));
                var factor = Convert.ToDouble(_session.premiInvestmentData.CaraBayar);
                var annFactor = obj.GetAnnuityFactor(basicTerm, null, product.ProductType);
                var upR = premi * factor * annFactor.Rate;
                var cor = (rate.Rate / 1000) * upR;
                return Json(new
                {
                    upR,
                    cor
                });
            }
        }
        #endregion

        #region Unit Link
        public virtual JsonResult CalculateUnitLinkCOR(decimal up, string riderCode, int? riderType, int age, int? riskClass, string riderCategory, int? unit)
        {
            SummaryViewModel dataInput = new SummaryViewModel();
            var product = obj.GetProduct(_session.dataNasabah.NamaProduk);
            if (riderCategory.Equals("Choices"))
            {
                up = CalculatorServices.CurrencyToDecimal(_session.premiInvestmentData.PremiBerkala) + CalculatorServices.CurrencyToDecimal(_session.premiInvestmentData.TopupBerkala);

                if (riderCode.Equals("POP"))
                    age = Convert.ToInt32(_session.dataNasabah.UmurPemegangPolis);

                var cor = MainServices.CalculateCostOfRider(dataInput, up, riderCode, riderType, age, riskClass, riderCategory, product, unit);

                return Json(new
                {
                    cor = cor,
                    upR = up
                });
            }
            else if (riderCategory.Equals("Unit"))
            {
                var cor = MainServices.CalculateCostOfRider(dataInput, up, riderCode, riderType, age, riskClass, riderCategory, product, unit);

                return Json(cor);
            }
            else
            {
                var cor = MainServices.CalculateCostOfRider(dataInput, up, riderCode, riderType, age, riskClass, riderCategory, product, unit);

                return Json(cor);
            }
        }

        public virtual JsonResult CalculateCOI(string up)
        {
            SummaryViewModel dataInput = new SummaryViewModel();
            dataInput.Premi = new PremiumAndInvestmentViewModel();

            dataInput.Nasabah = _session.dataNasabah;
            dataInput.Premi.UangPertanggungan = up;

            var coi = MainServices.CalculateCostOfInsurance(dataInput);

            return Json(coi);
        }
        public virtual JsonResult GetSAMultiplier(string productCode,int age, string type)
        {
            var sa_mult = obj.GetSAMultiplier(productCode, age, type);
            var ret = sa_mult == null ? 0 : sa_mult.Multiplier; 
            return Json(ret);
        }
        #endregion
    }
}
