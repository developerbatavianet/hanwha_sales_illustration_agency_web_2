﻿using Sales.Illustration.Web.Domain;
using Sales.Illustration.Web.Helper;
using Sales.Illustration.Web.Models;
using Sales.Illustration.Web.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Globalization;

namespace Sales.Illustration.Web.Controllers
{
    [SessionExpire]
    public class HistoryController : Controller
    {
        MainModel obj = new MainModel();

        public ActionResult Index()
        {
            ViewData["agentCode"] = _session.AgentCode;

            return View();
        }

        public ActionResult LoadHistoryData(string id)
        {
            var transLog = obj.GetTransLog(id);
            var nasabahData = transLog.NasabahDatas.FirstOrDefault(x => x.TransCode == transLog.TransCode);
            var premiData = transLog.PremiumDatas.FirstOrDefault(x => x.TransCode == transLog.TransCode);
            var fundData = transLog.InvestmentDatas.Where(x => x.TransCode == transLog.TransCode).ToList();
            var riderData = transLog.RiderDatas.Where(x => x.TransCode == transLog.TransCode).ToList();
            var addInsData = transLog.AdditionalInsuredDatas.Where(x => x.TransCode == transLog.TransCode).ToList();
            var topupData = transLog.TopUpWithdrawalDatas.Where(x => x.TransCode == transLog.TransCode).ToList();

            var product = obj.GetProduct(nasabahData.ProductCode);
            var nasabah = _session.dataNasabah = new ViewModel.NasabahDataViewModel();
            var premi = _session.premiInvestmentData = new ViewModel.PremiumAndInvestmentViewModel(); ;
            var rider = _session.riderData = new ViewModel.RiderViewModel(); ;
            var topup = _session.topUpWithData = new ViewModel.TopUpWithdrawalViewModel();
            var ratio = _session.insCostRatioData = new ViewModel.InsuranceCostAndRatioViewModel();

            _session.TransLogCode = transLog.TransCode;

            #region Nasabah
            nasabah.NamaPemegangPolis = nasabahData.PolicyHolderName;
            if(product.ProductCode != "HLWORK")
            {
            nasabah.TanggalLahirPemegangPolis = Convert.ToDateTime(nasabahData.PolicyHolderDOB, CultureInfo.InvariantCulture).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);

            if (nasabahData.PolicyHolderGender.Equals("F"))
                nasabah.JenisKelaminPemegangPolis = "Wanita";
            else
                nasabah.JenisKelaminPemegangPolis = "Pria";

            nasabah.StatusPekerjaanPemegangPolis = nasabahData.PolicyHolderRiskClassId.ToString();
            var riskPolicyHolder = obj.GetRiskClass((int)nasabahData.PolicyHolderRiskClassId);
            nasabah.KelasPekerjaanPemegangPolis = riskPolicyHolder.Class.ToString();
            nasabah.UmurPemegangPolis = CalculatorServices.CalculateAge(Convert.ToDateTime(nasabahData.PolicyHolderDOB, CultureInfo.InvariantCulture));
            }

            if (nasabahData.IsMainInsured == true)
                nasabah.TertanggungUtama = "Ya";
            else
            {
                nasabah.TertanggungUtama = "Tidak";
                nasabah.NamaTertanggungUtama = nasabahData.InsuredName;
                nasabah.TanggalLahirTertanggungUtama = Convert.ToDateTime(nasabahData.InsuredDOB, CultureInfo.InvariantCulture).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);

                if (nasabahData.InsuredGender.Equals("F"))
                    nasabah.JenisKelaminTertanggungUtama = "Wanita";
                else
                    nasabah.JenisKelaminTertanggungUtama = "Pria";

                nasabah.StatusPekerjaanTertanggungUtama = nasabahData.InsuredRiskClassId.ToString();
                var riskMainInsured = obj.GetRiskClass((int)nasabahData.InsuredRiskClassId);
                nasabah.KelasPekerjaanTertanggungUtama = riskMainInsured.Class.ToString();
                nasabah.UmurTertanggungUtama = CalculatorServices.CalculateAge(Convert.ToDateTime(nasabahData.InsuredDOB, CultureInfo.InvariantCulture));
                nasabah.Relation = nasabahData.Relation;
            }

            nasabah.NamaProduk = nasabahData.ProductCode;
            ratio.Language = nasabahData.Language;
            #endregion

            #region Premi
            premi.MataUang = premiData.Currency;
            premi.TotalInvestment = 100;

            if (premiData.PaymentMethod != null)
                premi.CaraBayar = premiData.PaymentMethod.ToString();

            premi.PremiAngsuran = CalculatorServices.DecimalToCurrency((decimal)premiData.RegularPremium + (premiData.RegularTopUp != null ? (decimal)premiData.RegularTopUp : 0));
            premi.PremiBerkala = CalculatorServices.DecimalToCurrency((decimal)premiData.RegularPremium);
            premi.UangPertanggungan = CalculatorServices.DecimalToCurrency((decimal)premiData.SumInsured);
            #endregion

            #region Rider
            var riders = obj.GetRiderByProduct(nasabah.NamaProduk).Where(x => x.RiderCode != "SP").GroupBy(x => x.RiderCode).Select(x => x.First());
            rider.Riders = new List<RiderItem>();
            decimal totalBiayaAsuransi = 0;

            foreach (var riderItem in riders)
            {
                List<Choices> choices = new List<Choices>();

                if (riderItem.Category.Equals("Choices"))
                {
                    var riderTypeList = obj.GetRiderProduct(product.ProductCode, riderItem.RiderCode).Where(x => x.RiderTypeId != null).Select(x => x.RiderTypeId).ToList();

                    foreach (var c in riderItem.RiderTypes.Where(x => riderTypeList.Contains(x.RiderTypeId)))
                    {
                        var choice = new Choices();
                        choice.RiderType = c;
                        choice.Checked = false;
                        choices.Add(choice);
                    }
                }

                rider.Riders.Add(new RiderItem()
                {
                    Rider = riderItem,
                    Choices = choices
                });
            }

            foreach (var riderItem in rider.Riders)
            {
                var riderDataItem = riderData.FirstOrDefault(x => x.RiderCode == riderItem.Rider.RiderCode);

                if (riderDataItem != null)
                {
                    riderItem.Checked = true;
                    if (!riderItem.Rider.Category.Equals("Choices"))
                    {
                        riderItem.BiayaAsuransi = CalculatorServices.DecimalToCurrency((decimal)riderDataItem.COR);
                        totalBiayaAsuransi += (decimal)riderDataItem.COR;
                    }

                    if (riderDataItem.SumInsured != null)
                        riderItem.UangPertanggungan = CalculatorServices.DecimalToCurrency((decimal)riderDataItem.SumInsured);

                    if (riderDataItem.AllocationPerc != null)
                        riderItem.AllocationPercent = riderDataItem.AllocationPerc + "%";

                    if (riderDataItem.RiderTypeId != null)
                    {
                        if (riderItem.Rider.Category.Equals("Choices"))
                        {
                            decimal choicesCOR = 0;
                            foreach (var choice in riderItem.Choices)
                            {
                                var choicheData = riderData.Where(x => x.RiderCode == riderDataItem.RiderCode).ToList();

                                foreach (var cho in choicheData)
                                {
                                    if (choice.RiderType.RiderTypeId == cho.RiderTypeId)
                                    {
                                        choice.RiderType.RiderTypeId = (int)cho.RiderTypeId;
                                        choice.Checked = true;
                                        choicesCOR += (decimal)cho.COR;
                                        totalBiayaAsuransi += (decimal)cho.COR;
                                    }
                                }
                            }
                            riderItem.BiayaAsuransi = CalculatorServices.DecimalToCurrency(choicesCOR);
                        }
                        else if (riderItem.Rider.Category.Equals("Unit"))
                        {
                            riderItem.UangPertanggungan = Int32.Parse(riderItem.UangPertanggungan) > 0 ? riderItem.UangPertanggungan : null;
                            foreach (var unit in riderItem.Rider.RiderTypes)
                            {
                                if (unit.RiderTypeId == riderDataItem.RiderTypeId)
                                {
                                    riderItem.UnitName = riderDataItem.RiderTypeId.ToString();
                                    riderItem.Unit = riderDataItem.Unit.ToString();
                                }
                            }
                        }
                        else if (riderItem.Rider.Category.Equals("Choice"))
                        {
                            riderItem.UangPertanggungan = Int32.Parse(riderItem.UangPertanggungan) > 0 ? riderItem.UangPertanggungan : null;
                            foreach (var choice in riderItem.Rider.RiderTypes)
                            {
                                if (choice.RiderTypeId == riderDataItem.RiderTypeId)
                                {
                                    riderItem.Choice = riderDataItem.RiderTypeId.ToString();
                                }
                            }
                        }
                    }
                }
            }

            rider.BiayaAsuransiTambahan = CalculatorServices.DecimalToCurrency(totalBiayaAsuransi);
            #endregion

            #region Additional Insured
            decimal totalBiayaAsuransiTambahan = 0;
            if (product.ProductType.Equals("tra"))
            {
                var addIns = _session.addInsTraditionalData = new ViewModel.AdditionalInsuredTraditionalViewModel();
                riders = obj.GetRiderByProduct(nasabah.NamaProduk).Where(x => !x.AllowAdditionalInsured.Contains(product.ProductCode + "-NO")).GroupBy(x => x.RiderCode).Select(x => x.First());
                addIns.TertanggungTambahan = new List<AdditionalInsured>();

                foreach (var ins in addInsData)
                {
                    addIns.TertanggungTambahan.Add(new AdditionalInsured()
                    {
                        Age = ins.AdditionalInsuredAge,
                        Nama = ins.AdditionalInsuredName,
                        DateOfBirth = Convert.ToDateTime(ins.AdditionalInsuredDOB, CultureInfo.InvariantCulture).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                        Gender = ins.AdditionalInsuredGender.Contains("F") ? "Wanita" : "Pria",
                        KelasPekerjaan = ins.AdditionalInsuredRiskClassId.ToString(),
                        Relationship = ins.AdditionalInsuredRelation,
                        RiskClass = obj.GetRiskClass(Convert.ToInt32(ins.AdditionalInsuredRiskClassId)).Class.ToString()
                    });
                }
            }
            else
            {
                var addIns = _session.addInsData = new ViewModel.AdditionalInsuredViewModel();
                riders = obj.GetRiderByProduct(nasabah.NamaProduk).Where(x => !x.AllowAdditionalInsured.Contains(product.ProductCode + "-NO")).GroupBy(x => x.RiderCode).Select(x => x.First());
                addIns.TertanggungTambahan = new List<AdditionalInsured>();
                addIns.Riders = new List<RiderItem>();
                addIns.RiderNoData = new List<RiderItem>();

                foreach (var ins in addInsData)
                {
                    addIns.TertanggungTambahan.Add(new AdditionalInsured()
                    {
                        Age = ins.AdditionalInsuredAge,
                        Nama = ins.AdditionalInsuredName,
                        DateOfBirth = Convert.ToDateTime(ins.AdditionalInsuredDOB, CultureInfo.InvariantCulture).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                        Gender = ins.AdditionalInsuredGender.Contains("F") ? "Wanita" : "Pria",
                        KelasPekerjaan = ins.AdditionalInsuredRiskClassId.ToString(),
                        Relationship = ins.AdditionalInsuredRelation,
                        RiskClass = obj.GetRiskClass(Convert.ToInt32(ins.AdditionalInsuredRiskClassId)).Class.ToString()
                    });
                }

                foreach (var riderItem in riders)
                {
                    List<Choices> choices = new List<Choices>();

                    if (riderItem.Category.Equals("Choices"))
                    {
                        var riderTypeList = obj.GetRiderProduct(product.ProductCode, riderItem.RiderCode).Where(x => x.RiderTypeId != null).Select(x => x.RiderTypeId).ToList();

                        foreach (var c in riderItem.RiderTypes.Where(x => riderTypeList.Contains(x.RiderTypeId)))
                        {
                            var choice = new Choices();
                            choice.RiderType = c;
                            choice.Checked = false;
                            choices.Add(choice);
                        }
                    }

                    addIns.RiderNoData.Add(new RiderItem()
                    {
                        Rider = riderItem,
                        Choices = choices
                    });
                }

                for (int i = 0; i < addIns.RiderNoData.Count; i++)
                {
                    for (int y = 0; y < addInsData.Count; y++)
                    {
                        var riderItem = addIns.RiderNoData[i];

                        List<Choices> choices = new List<Choices>();

                        if (riderItem.Rider.Category.Equals("Choices"))
                        {
                            var riderTypeList = obj.GetRiderProduct(product.ProductCode, riderItem.Rider.RiderCode).Where(x => x.RiderTypeId != null).Select(x => x.RiderTypeId).ToList();

                            foreach (var c in riderItem.Rider.RiderTypes.Where(x => riderTypeList.Contains(x.RiderTypeId)))
                            {
                                var choice = new Choices();
                                choice.RiderType = c;
                                choice.Checked = false;
                                choices.Add(choice);
                            }
                        }

                        addIns.Riders.Add(new RiderItem()
                        {
                            Rider = riderItem.Rider,
                            Choices = choices
                        });
                    }
                }

                for (int i = 0; i < addInsData.Count; i++)
                {
                    for (int y = i; y < addIns.Riders.Count; y = y + addInsData.Count)
                    {
                        var addInsRiderData = addInsData[i].AdditionalInsuredRiderDatas.Where(x => x.AdditionalInsuredDataId == addInsData[i].AdditionalInsuredDataId).ToList();
                        var riderItem = addIns.Riders[y];
                        var riderDataItems = addInsRiderData.Where(x => x.RiderCode == riderItem.Rider.RiderCode && x.AdditionalInsuredDataId == addInsData[i].AdditionalInsuredDataId).ToList();

                        if (riderDataItems != null)
                        {
                            decimal biayaAsuransi = 0;
                            foreach (var riderDataItem in riderDataItems)
                            {
                                riderItem.Checked = true;
                                biayaAsuransi += (decimal)riderDataItem.COR;
                                riderItem.BiayaAsuransi = CalculatorServices.DecimalToCurrency(biayaAsuransi);
                                totalBiayaAsuransiTambahan += (decimal)riderDataItem.COR;

                                if (riderDataItem.SumInsured != null)
                                    riderItem.UangPertanggungan = CalculatorServices.DecimalToCurrency((decimal)riderDataItem.SumInsured);

                                if (riderDataItem.AllocationPerc != null)
                                    riderItem.AllocationPercent = riderDataItem.AllocationPerc + "%";

                                if (riderDataItem.RiderTypeId != null)
                                {
                                    if (riderItem.Rider.Category.Equals("Choices"))
                                    {
                                        foreach (var choice in riderItem.Choices)
                                        {
                                            if (choice.RiderType.RiderTypeId == riderDataItem.RiderTypeId)
                                            {
                                                choice.RiderType.RiderTypeId = (int)riderDataItem.RiderTypeId;
                                                choice.Checked = true;
                                            }
                                        }
                                    }
                                    if (riderItem.Rider.Category.Equals("Choices"))
                                    {
                                        foreach (var choice in riderItem.Choices)
                                        {
                                            var choicheData = addInsRiderData.Where(x => x.RiderCode == riderItem.Rider.RiderCode && x.AdditionalInsuredDataId == addInsData[i].AdditionalInsuredDataId).ToList();

                                            foreach (var cho in choicheData)
                                            {
                                                if (choice.RiderType.RiderTypeId == cho.RiderTypeId)
                                                {
                                                    choice.RiderType.RiderTypeId = (int)cho.RiderTypeId;
                                                    choice.Checked = true;
                                                    choice.BiayaAsuransi = cho.COR.ToString();
                                                }
                                            }
                                        }
                                    }
                                    else if (riderItem.Rider.Category.Equals("Unit"))
                                    {
                                        riderItem.UangPertanggungan = Int32.Parse(riderItem.UangPertanggungan) > 0 ? riderItem.UangPertanggungan : null;
                                        foreach (var unit in riderItem.Rider.RiderTypes)
                                        {
                                            if (unit.RiderTypeId == riderDataItem.RiderTypeId)
                                            {
                                                riderItem.UnitName = riderDataItem.RiderTypeId.ToString();
                                                riderItem.Unit = riderDataItem.Unit.ToString();
                                            }
                                        }
                                    }
                                    else if (riderItem.Rider.Category.Equals("Choice"))
                                    {
                                        riderItem.UangPertanggungan = Int32.Parse(riderItem.UangPertanggungan) > 0 ? riderItem.UangPertanggungan : null;
                                        foreach (var choice in riderItem.Rider.RiderTypes)
                                        {
                                            if (choice.RiderTypeId == riderDataItem.RiderTypeId)
                                            {
                                                riderItem.Choice = riderDataItem.RiderTypeId.ToString();
                                            }
                                        }
                                    }
                                }
                            }

                        }
                    }
                }
                addIns.BiayaAsuransiTertanggungTambahan = CalculatorServices.DecimalToCurrency(totalBiayaAsuransiTambahan);
            }
            #endregion

            #region Fund
            premi.Investments = obj.GetFundByProduct(nasabah.NamaProduk).Select(x => new Domain.Investment() { InvestmentName = x.FundName, InvestmentCode = x.FundCode }).ToList();

            foreach (var fundItem in premi.Investments)
            {
                var fund = fundData.FirstOrDefault(x => x.FundCode == fundItem.InvestmentCode);

                if (fund != null)
                {
                    fundItem.InvestmentCode = fund.FundCode;
                    fundItem.Percentage = (int)fund.FundValue;
                }
            }
            #endregion

            #region TopUpWithdrawal
            topup.TopupWithdrawals = topupData.Select(x => new Domain.TopUpWithdrawal() { IsTopUp = (bool)x.IsTopUp, Year = (int)x.TransYear, Amount = CalculatorServices.DecimalToCurrency((decimal)x.TransAmount) }).ToList();
            #endregion

            if (product.ProductType.Equals("tra"))
            {
                #region Anak
                if (!String.IsNullOrEmpty(nasabahData.ChildName))
                {
                    nasabah.NamaAnak = nasabahData.ChildName;
                    nasabah.TanggalLahirAnak = Convert.ToDateTime(nasabahData.ChildDOB, CultureInfo.InvariantCulture).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);

                    if (nasabahData.ChildGender.Equals("F"))
                        nasabah.JenisKelaminAnak = "Wanita";
                    else
                        nasabah.JenisKelaminAnak = "Pria";

                    nasabah.UmurAnak = (int)nasabahData.ChildAge;
                }
                #endregion

                if (premiData.PaymentPeriodOption > 0)
                    premi.PilihanMasaPembayaran = premiData.PaymentPeriodOption.ToString();

                if (!String.IsNullOrEmpty(premiData.PaymentMode))
                    premi.ModeBayarPremi = premiData.PaymentMode;

                if (premiData.InsurancePeriod > 0)
                    premi.MasaAsuransi = premiData.InsurancePeriod;

                if (premiData.PaymentPeriod > 0)
                    premi.RencanaMasaPembayaran = (int)premiData.PaymentPeriod;

                premi.UsiaMasukAnak = nasabahData.ChildAge;
                premi.BiayaAsuransi = CalculatorServices.DecimalToCurrency((decimal)premiData.RegularPremium);
                ratio.BiayaAsuransiTotal = CalculatorServices.DecimalToCurrency((decimal)premiData.RegularPremium + (decimal)totalBiayaAsuransi);

                return RedirectToAction("Summary", "Traditional");
            }
            else
            {
                ratio.RatioPremiBerkala = premiData.RatioRegularPremium.ToString() + "%";

                if (premiData.PaymentPeriod != null)
                    premi.RencanaMasaPembayaran = (int)premiData.PaymentPeriod;

                if (premiData.RegularTopUp != null)
                    premi.TopupBerkala = CalculatorServices.DecimalToCurrency((decimal)premiData.RegularTopUp);

                premi.UangPertanggungan = CalculatorServices.DecimalToCurrency((decimal)premiData.SumInsured);
                premi.BiayaAsuransi = CalculatorServices.DecimalToCurrency((decimal)premiData.CostOfInsurance);
                ratio.BiayaAsuransiTotal = CalculatorServices.DecimalToCurrency((decimal)premiData.CostOfInsurance + (decimal)totalBiayaAsuransi + totalBiayaAsuransiTambahan);

                if (premiData.UnitWizer != null)
                    premi.UnitWizer = premiData.UnitWizer;

                return RedirectToAction("Summary", "UnitLink");
            }
        }
    }
}
