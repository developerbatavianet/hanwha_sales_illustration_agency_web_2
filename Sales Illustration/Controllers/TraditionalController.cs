﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using Sales.Illustration.Web.Domain;
using Sales.Illustration.Web.Extensions;
using Sales.Illustration.Web.Helper;
using Sales.Illustration.Web.Services;
using Sales.Illustration.Web.ViewModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace Sales.Illustration.Web.Controllers
{
    [SessionExpire]
    public class TraditionalController : Controller
    {
        Sales.Illustration.Web.Models.MainModel obj = new Sales.Illustration.Web.Models.MainModel();

        //
        // GET: /Traditional/

        public ActionResult Index()
        {
            _session.dataNasabah = null;
            _session.premiInvestmentData = null;
            _session.riderData = null;
            _session.insCostRatioData = null;
            _session.TransLogCode = null;
            _session.addInsTraditionalData = null;

            return RedirectToAction("NasabahData");
        }

        public ActionResult NasabahData()
        {
            var model = new NasabahDataViewModel();

            if (_session.dataNasabah != null)
                model = _session.dataNasabah;

            ViewData["riskClass"] = obj.GetActiveRiskClass().Select(x => new SelectListItem { Text = x.RiskClassDesc, Value = x.RiskClassId.ToString() });
            ViewData["products"] = obj.GetProductByType("tra").Select(x => new SelectListItem { Text = x.ProductName, Value = x.ProductCode });

            return View("NasabahData", model);
        }

        [HttpPost]
        public ActionResult ValidateNasabahData(NasabahDataViewModel data)
        {
            if (data.TertanggungUtama == "Ya")
            {
                data.NamaTertanggungUtama = null;
                data.JenisKelaminTertanggungUtama = null;
                data.TanggalLahirTertanggungUtama = null;
                data.StatusPekerjaanTertanggungUtama = null;
                data.KelasPekerjaanTertanggungUtama = null;
                data.UmurTertanggungUtama = null;
                data.Relation = null;
            }

            _session.dataNasabah = data;

            return RedirectToAction("PremiumAndInvestment");
        }

        public ActionResult BackToNasabahData()
        {
            if (_session.premiInvestmentData != null)
                _session.premiInvestmentData = null;

            return RedirectToAction("NasabahData");
        }

        public ActionResult PremiumAndInvestment()
        {
            var model = new PremiumAndInvestmentViewModel();

            if (_session.premiInvestmentData != null)
            {
                model = _session.premiInvestmentData;
                model.additionalInsured = _session.addInsTraditionalData;
            }
            else
            {
                model.additionalInsured = new AdditionalInsuredTraditionalViewModel();
                model.additionalInsured.TertanggungTambahan = new List<AdditionalInsured>();
                var insured = new AdditionalInsured();
                model.additionalInsured.TertanggungTambahan.Add(insured);
            }

            var insAge = 0;
            if (_session.dataNasabah.TertanggungUtama == "Ya")
                insAge = Convert.ToInt32(_session.dataNasabah.UmurPemegangPolis);
            else
                insAge = Convert.ToInt32(_session.dataNasabah.UmurTertanggungUtama);

            model.additionalInsured.NasabahData = _session.dataNasabah;
            model.MataUang = "Rupiah";
            ViewData["insAge"] = insAge;
            ViewData["isTertanggungUtama"] = _session.dataNasabah.TertanggungUtama;
            ViewData["riskClass"] = obj.GetActiveRiskClass().Select(x => new SelectListItem { Text = x.RiskClassDesc, Value = x.RiskClassId.ToString() });
            model.UsiaMasukAnak = _session.dataNasabah.UmurAnak;
            ViewData["paymentMethods"] = obj.GetActivePaymentMethod().Select(x => new SelectListItem { Text = x.PMName, Value = x.PMCode.ToString() });
            var product = obj.GetProduct(_session.dataNasabah.NamaProduk);
            ViewData["ProductTitle"] = product.ProductName;
            ViewData["ProductCode"] = product.ProductCode;
            var pekerjaanP = obj.GetRiskClass(Convert.ToInt32(model.additionalInsured.NasabahData.StatusPekerjaanPemegangPolis));
            ViewData["pekerjaanP"] = pekerjaanP.RiskClassDesc;
            var pekerjaanT = obj.GetRiskClass(Convert.ToInt32(model.additionalInsured.NasabahData.StatusPekerjaanTertanggungUtama));
            ViewData["pekerjaanT"] = model.additionalInsured.NasabahData.TertanggungUtama == "Ya" ? "" : pekerjaanT.RiskClassDesc;

            return View("PremiumAndInvestment", model);
        }

        [HttpPost]
        public ActionResult ValidatePremiumAndInvestmentData(PremiumAndInvestmentViewModel data,AdditionalInsuredTraditionalViewModel addins)
        {
            var redirect = "Rider";
            var product = obj.GetProduct(_session.dataNasabah.NamaProduk);
            _session.premiInvestmentData = data;
            _session.addInsTraditionalData = addins;

            if (product.ProductCode != "HLEDU")
                redirect = "InsuranceCostAndRatio";

            if ((_session.dataNasabah.TertanggungUtama == "Ya" && _session.dataNasabah.UmurPemegangPolis <= 16) ||
                (_session.dataNasabah.TertanggungUtama != "Ya" && _session.dataNasabah.UmurTertanggungUtama <= 16))
            {
                if (product.ProductCode == "HLLIFE" && CalculatorServices.CurrencyToDecimal(data.UangPertanggungan) > 1500000000)
                {
                    TempData["errorMsg"] = "Maksimum Uang Pertanggungan untuk Tertanggung Utama Anak (Juvenile) adalah Rp 1.500.000.000";
                    return RedirectToAction("PremiumAndInvestment");
                }
            }

            return RedirectToAction(redirect);
        }

        public ActionResult BackToPremiumAndInvestment()
        {
            var product = obj.GetProduct(_session.dataNasabah.NamaProduk);
            if (_session.riderData != null)
                _session.riderData = null;

            if (_session.addInsData != null)
                _session.addInsData = null;

            if (product.ProductCode != "HLEDU" && _session.insCostRatioData != null)
                _session.insCostRatioData = null;

            return RedirectToAction("PremiumAndInvestment");
        }

        public ActionResult Rider()
        {
            var model = new RiderViewModel();

            if (_session.riderData != null)
            {
                model = _session.riderData;
            }
            else
            {
                var riders = obj.GetRiderByProduct(_session.dataNasabah.NamaProduk).GroupBy(x => x.RiderCode).Select(x => x.First());
                model.Riders = new List<Domain.RiderItem>();
                var totalBiayaAsuransi = 0;

                foreach (var rider in riders)
                {
                    List<Choices> choices = new List<Choices>();
                    var prod = obj.GetProduct(_session.dataNasabah.NamaProduk);

                    var check = prod.ProductCategory.Equals("wizer") && (rider.RiderCode == "WOP" || rider.RiderCode == "ADB");
                    var uangPertanggungan = prod.ProductCategory.Equals("wizer") && (rider.RiderCode == "ADB") ? 3 * Int32.Parse(_session.premiInvestmentData.UangPertanggungan.Replace(",", "")) : 0;
                    var allocationPercent = prod.ProductCategory.Equals("wizer") && (rider.RiderCode == "ADB") ? "300%" : "0%";
                    var biayaAsuransi = prod.ProductCategory.Equals("wizer") && (rider.RiderCode == "ADB") ?
                        (int)MainServices.CalculateCostOfRider(new SummaryViewModel(), uangPertanggungan, rider.RiderCode,
                        null, _session.dataNasabah.UmurPemegangPolis, Int32.Parse(_session.dataNasabah.KelasPekerjaanPemegangPolis), rider.Category, prod, null) : 0;

                    var riderTypeList = obj.GetRiderProduct(prod.ProductCode, rider.RiderCode).Where(x => x.RiderTypeId != null).Select(x => x.RiderTypeId).ToList();

                    foreach (var c in rider.RiderTypes.Where(x => riderTypeList.Contains(x.RiderTypeId)))
                    {
                        var choice = new Choices();
                        choice.Checked = c.TypeName == "CI" && prod.ProductCategory.Equals("wizer");
                        choice.RiderType = c;

                        uangPertanggungan = choice.Checked ? Int32.Parse(_session.premiInvestmentData.UangPertanggungan.Replace(",", "")) : 0;
                        var dataInput = new SummaryViewModel();
                        var riderCode = rider.RiderCode;
                        var riderType = c.RiderTypeId;
                        var age1 = _session.dataNasabah.UmurPemegangPolis;
                        var riderCategory = rider.Category;
                        biayaAsuransi += choice.Checked ? (int)MainServices.CalculateCostOfRider(dataInput, uangPertanggungan, riderCode, riderType, age1, null, riderCategory, prod, null) : 0;

                        choices.Add(choice);
                    }

                    totalBiayaAsuransi += biayaAsuransi;
                    model.Riders.Add(new Domain.RiderItem() { Rider = rider, Checked = check, Choices = choices, BiayaAsuransi = string.Format("{0:n0}", biayaAsuransi).Replace(".", ","), UangPertanggungan = uangPertanggungan > 0 ? string.Format("{0:n0}", uangPertanggungan).Replace(".", ",") : "", AllocationPercent = allocationPercent });
                }

                model.BiayaAsuransiTambahan = string.Format("{0:n0}", totalBiayaAsuransi);
            }

            var insAge = 0;
            if (_session.dataNasabah.TertanggungUtama == "Ya")
                insAge = Convert.ToInt32(_session.dataNasabah.UmurPemegangPolis);
            else
                insAge = Convert.ToInt32(_session.dataNasabah.UmurTertanggungUtama);

            ViewData["up"] = _session.premiInvestmentData.UangPertanggungan;
            ViewData["ttgUtama"] = _session.dataNasabah.TertanggungUtama;
            ViewData["modebayar"] = _session.premiInvestmentData.ModeBayarPremi;
            ViewData["masaasuransi"] = _session.premiInvestmentData.MasaAsuransi;
            ViewData["masabayar"] = _session.premiInvestmentData.RencanaMasaPembayaran;
            ViewData["carabayar"] = _session.premiInvestmentData.CaraBayar;
            ViewData["insage"] = insAge;
            var product = obj.GetProduct(_session.dataNasabah.NamaProduk);
            ViewData["ProductTitle"] = product.ProductName;

            model.NasabahData = _session.dataNasabah;

            return View("Rider", model);
        }

        [HttpPost]
        public ActionResult ValidateRiderData(RiderViewModel data)
        {
            _session.riderData = data;

            return RedirectToAction("InsuranceCostAndRatio");
        }

        public ActionResult BackToRider()
        {
            if (_session.insCostRatioData != null)
                _session.insCostRatioData = null;

            return RedirectToAction("Rider");
        }

        public ActionResult InsuranceCostAndRatio()
        {
            var model = new InsuranceCostAndRatioViewModel();
            var modelTtg = new InsuranceCostTraditionalViewModel();
            var product = obj.GetProduct(_session.dataNasabah.NamaProduk);

            if (_session.insCostRatioData != null)
                model = _session.insCostRatioData;

            model.BiayaAsuransiTambahan = "0";
            if (_session.riderData != null)
                model.BiayaAsuransiTambahan = _session.riderData.BiayaAsuransiTambahan != null ? _session.riderData.BiayaAsuransiTambahan  : "0";

            if(product.ProductCode == "HLLIFE")
            {
                    var paymentMethod = obj.GetPaymentMethod(Int32.Parse(_session.premiInvestmentData.CaraBayar));
                    ViewData["carabayar"] = paymentMethod.PMName;
                var getAllTertanggung = MainServices.CalculatePremiAllTertanggung(product.SantunanDuka.ToString(),_session.premiInvestmentData.UangPertanggungan
                    ,_session.dataNasabah.UmurPemegangPolis,_session.dataNasabah.UmurTertanggungUtama,paymentMethod.Factor,_session.premiInvestmentData.RencanaMasaPembayaran,
                    _session.dataNasabah.TertanggungUtama == "Ya");
                ViewData["dataTertanggung"] = getAllTertanggung;
                model.BiayaAsuransiTotal = getAllTertanggung.TotalPremi;
            }

            ViewData["language"] = obj.GetActiveLanguage().Select(x => new SelectListItem { Text = x.Name, Value = x.CultureId });
            model.Premi = _session.premiInvestmentData.PremiBerkala;
            ViewData["ProductTitle"] = product.ProductName;
            ViewData["ProductCode"] = product.ProductCode;

            return View("InsuranceCostAndRatio", model);
        }

        [HttpPost]
        public ActionResult ValidateInsuranceCostAndRatioData(InsuranceCostAndRatioViewModel data)
        {
            _session.insCostRatioData = data;

            return RedirectToAction("Summary");
        }

        public ActionResult Summary()
        {
            var model = new SummaryViewModel();

            if (_session.dataNasabah != null)
                model.Nasabah = _session.dataNasabah;
            if (_session.premiInvestmentData != null)
                model.Premi = _session.premiInvestmentData;
            if (_session.riderData != null)
                model.Rider = _session.riderData;
            if (_session.insCostRatioData != null)
                model.Insurance = _session.insCostRatioData;
            if (_session.addInsTraditionalData != null)
                model.Premi.additionalInsured = _session.addInsTraditionalData;

            if (model.Nasabah.TertanggungUtama.Equals("Tidak"))
            {
                var pekerjaanT = obj.GetRiskClass(Convert.ToInt32(model.Nasabah.StatusPekerjaanTertanggungUtama));
                ViewData["pekerjaanT"] = pekerjaanT.RiskClassDesc;
            }
            var produk = obj.GetProduct(model.Nasabah.NamaProduk);

            if (produk.ProductCode == "HLLIFE")
            {
                var paymentMetod = model.Premi.CaraBayar == null ? null : obj.GetPaymentMethod(Int32.Parse(model.Premi.CaraBayar));
                var getAllTertanggung = MainServices.CalculatePremiAllTertanggung(produk.SantunanDuka.ToString(), model.Premi.UangPertanggungan
                    , model.Nasabah.UmurPemegangPolis, model.Nasabah.UmurTertanggungUtama, paymentMetod.Factor, model.Premi.RencanaMasaPembayaran,
                    model.Nasabah.TertanggungUtama == "Ya");
                ViewData["dataTertanggung"] = getAllTertanggung;
            }
            var pekerjaanP = obj.GetRiskClass(Convert.ToInt32(model.Nasabah.StatusPekerjaanPemegangPolis));
            ViewData["pekerjaanP"] = pekerjaanP.RiskClassDesc;
            ViewData["produk"] = produk.ProductName;
            ViewData["produkCode"] = produk.ProductName;
            var lang = obj.GetLanguage(model.Insurance.Language);
            ViewData["language"] = lang.Name;
            ViewData["caraBayar"] = model.Premi.CaraBayar == null ? null : obj.GetPaymentMethod(Int32.Parse(model.Premi.CaraBayar)).PMName;
            ViewData["premiTitle"] = model.Nasabah.NamaProduk.Equals("HLEDU") ? "Premi dan Uang Pertanggungan" : model.Nasabah.NamaProduk.Equals("HLKIDS")
                ? "Premi, Uang Pertanggungan dan Pilihan Bahasa" : "Masa Asuransi dan Uang Pertanggungan";

            return View("Summary", model);
        }
        public ActionResult AddAdditionalInsuredRow()
        {
            var additional = new AdditionalInsured();
            var prod = obj.GetProduct(_session.dataNasabah.NamaProduk);
            ViewData["ProductCode"] = prod.ProductCode;
            ViewData["riskClass"] = obj.GetActiveRiskClass().Select(x => new SelectListItem { Text = x.RiskClassDesc, Value = x.RiskClassId.ToString() });
            return PartialView("AdditionalInsuredRow", additional);
        }

        public ActionResult Report()
        {
            SummaryViewModel dataInput = new SummaryViewModel();
            dataInput.Nasabah = _session.dataNasabah;
            dataInput.Premi = _session.premiInvestmentData;
            dataInput.Rider = _session.riderData;
            dataInput.Insurance = _session.insCostRatioData;
            dataInput.Premi.additionalInsured = _session.addInsTraditionalData;

            if (String.IsNullOrEmpty(_session.TransLogCode))
            {
                var transLog = MainServices.CreateTransLog(dataInput, _session.AgentCode);
                if (!String.IsNullOrEmpty(transLog.TransCode))
                    _session.TransLogCode = transLog.TransCode;
            }
            else
            {
                MainServices.UpdateTransLog(dataInput, _session.AgentCode, _session.TransLogCode);
            }

            Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo(dataInput.Insurance.Language);

            var result = MainServices.GenerateReportTraditional(dataInput, _session.AgentName, _session.AgentCode, _session.AppVersion);
            if (result.ErrorMsg != null && result.ErrorMsg.Length > 0)
            {
                TempData["errorMsg"] = result.ErrorMsg[0];
                return RedirectToAction("Summary");
            }

            var rptH = new ReportDocument { FileName = Server.MapPath(string.Format("~/Report/{0}.rpt", result.ReportName)) };
            rptH.SetDataSource(result.IllustrationDataSet);

            var guid = _session.TransLogCode + ".pdf";
            var path = Path.Combine(Server.MapPath("~/Report/temp"), guid);
            rptH.ExportToDisk(ExportFormatType.PortableDocFormat, path);

            ViewData["path"] = Path.Combine("/Report/temp", guid);
            var product = obj.GetProduct(_session.dataNasabah.NamaProduk);
            ViewData["ProductTitle"] = product.ProductName;

            rptH.Close();
            rptH.Dispose();

            return View("Report");
        }

        [DeleteFile]
        public ActionResult ResetAllData()
        {
            return RedirectToAction("Index");
        }

        public ActionResult SendMailReport()
        {
            var model = new SendMailViewModel();
            var product = obj.GetProduct(_session.dataNasabah.NamaProduk);
            ViewData["productName"] = product.ProductName;

            if (_session.dataNasabah.TertanggungUtama.Equals("Ya"))
                model.Subject = "Ilustration " + product.ProductName + " - " + _session.dataNasabah.NamaPemegangPolis;
            else
                model.Subject = "Ilustration " + product.ProductName + " - " + _session.dataNasabah.NamaTertanggungUtama;

            model.Attachment = _session.TransLogCode + ".pdf";
            model.Source = product.ProductType;

            return View("SendMailReport", model);
        }
    }
}
