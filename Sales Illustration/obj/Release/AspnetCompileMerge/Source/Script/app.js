﻿$.ajaxSetup({
    beforeSend: function () {
        $("body").prepend("<div class=\"overlayajax\"><img class=\"imgsrc\" src=\"/Content/dist/img/load.gif\" /></div>");
    }
});
$(document).ajaxStop(function () {
    $(".overlayajax").remove();
});

function toDecimalFormat(value) {
    if (value != null) {
        if (value.length == 0) {
            return 0;
        }
        return value.toString().replace(/,/g, "");
    }
    return 0;
}

function toMoneyFormatWithElements(firstElement, secondElement, elementChanged) {
    firstElement.oninput = function () {
        var firstValue = this.value;
        firstValue = firstValue.replace(/,/g, "");

        var secondValue = secondElement.value.toString().replace(/,/g, "");
        var firstNumValue = firstValue != "" ? parseInt(firstValue) : 0;
        var secondNumValue = secondValue != "" ? parseInt(secondValue) : 0;
        var total = firstNumValue + secondNumValue;

        elementChanged.value = toMoneyFormat(total);

        firstElement.value = toMoneyFormat(firstValue);
    }
}

function zeroingValueOnBlur(element) {
    if (element.value == "") {
        element.value = 0;
    }
}

function clearValueOnFocus(element) {
    if (element.value == 0) {
        element.value = "";
    }
}

function zeroingPercentValueOnBlur(element) {
    if (element.value == "") {
        element.value = "0%";
    } else {
        element.value = element.value + "%";
    }
}

function clearPercentValueOnFocus(element) {
    if (element.value == "0%") {
        element.value = "";
    } else {
        element.value = element.value.replace("%", "");
    }
}

function toMoneyFormatElement(element) {
    var val = element.value.replace(/,/g, "");
    element.value = toMoneyFormat(val);
}

function toMoneyFormat(value) {
    return value.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
}

var roundNumber = function (number, digits) {
    var multiple = Math.pow(10, digits);
    var rndedNum = Math.round(number / multiple) * multiple;
    return rndedNum;
}

function isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}

function parseDateDiff(str) {
    var mdy = str.split('/');
    return new Date(mdy[2], mdy[1], mdy[0] - 1);
}

function daydiff(first, second) {
    return Math.round((second - first) / (1000 * 60 * 60 * 24));
}

function hideTertanggungUtama(x) {
    if (x.checked) {
        document.getElementById("namaInsDiv").style.display = 'none';
        document.getElementById("birthdayInsDiv").style.display = 'none';
        document.getElementById("genderInsDiv").style.display = 'none';
        document.getElementById("smokeStatusInsDiv").style.display = 'none';
        document.getElementById("workStatusInsDiv").style.display = 'none';
        //document.getElementById("workClassInsDiv").style.display = 'none';
        document.getElementById("ageInsDiv").style.display = 'none';
        document.getElementById("relation").style.display = 'none';
    } else {
        document.getElementById("namaInsDiv").style.display = 'block';
        document.getElementById("birthdayInsDiv").style.display = 'block';
        document.getElementById("genderInsDiv").style.display = 'block';
        document.getElementById("smokeStatusInsDiv").style.display = 'block';
        document.getElementById("workStatusInsDiv").style.display = 'block';
        //document.getElementById("workClassInsDiv").style.display = 'block';
        document.getElementById("ageInsDiv").style.display = 'block';
        document.getElementById("relation").style.display = 'block';
    }
}

function hidePemegangPolis(x,id) {
    if (x) {
        document.getElementById("birthdayDiv").style.display = 'none';
        document.getElementById("genderDiv").style.display = 'none';
        document.getElementById("smokeStatusDiv").style.display = 'none';
        document.getElementById("workStatusDiv").style.display = 'none';
        document.getElementById("ageDiv").style.display = 'none';
        document.getElementById("namaPH").innerHTML = 'Nama Perusahaan';
        document.getElementById(id).placeholder = 'Nama Perusahaan';
    } else {
        document.getElementById("birthdayDiv").style.display = 'block';
        document.getElementById("genderDiv").style.display = 'block';
        document.getElementById("smokeStatusDiv").style.display = 'block';
        document.getElementById("workStatusDiv").style.display = 'block';
        document.getElementById("ageDiv").style.display = 'block';
        document.getElementById("namaPH").innerHTML = 'Nama Pemegang Polis';
        document.getElementById(id).placeholder = 'Nama Pemegang Polis';
    }
}
function getKelasPekerjaan(id, listener) {
    $.ajax({
        url: '/Ajax/GetRiskClass',
        type: 'POST',
        dataType: 'json',
        data: { id: id },
        success: function (result) {
            listener(result["Class"]);
        },
        error: function () {
            listener("");
        }
    });
}
function toDoubleFormat(value) {
    if (value != null) {
        if (value.length == 0) {
            return 0;
        }
        return value.replace(/,/g, "");
    }
    return 0;
}

function parseDate(s) {
    var months = {
        january: 0, february: 1, march: 2, april: 3, may: 4, june: 5,
        july: 6, august: 7, september: 8, october: 9, november: 10, december: 11
    };
    var p = s.split(' ');
    return new Date(p[2], months[p[1].toLowerCase()], p[0]);
}

function calculateAge(dob) {
    var today = new Date();
    var age = today.getTime() - dob.getTime();
    age = Math.floor(age / (1000 * 60 * 60 * 24 * 365.25));
    return age;
}

function calcAge(dob, listener) {
    $.ajax({
        url: '/Ajax/CalculateAge',
        type: 'POST',
        dataType: 'json',
        data: { dob: dob },
        success: function (result) {
            listener(result);
        },
        error: function () {
            listener("");
        }
    });
}

function getInsurancePeriod(age,productCode,listener) {
    $.ajax({
        url: '/Ajax/GetInsPeriod',
        type: 'POST',
        dataType: 'json',
        data: { age: age ,productCode:productCode},
        success: function (result) {
            listener(result);
        },
        error: function () {
            listener("");
        }
    });
}

function getPaymentPeriod(age, paymentPeriod, paymentMode,productCode, listener) {
    $.ajax({
        url: '/Ajax/GetPaymentPeriod',
        type: 'POST',
        dataType: 'json',
        data: { age: age, paymentPeriod: paymentPeriod, paymentMode: paymentMode,productCode:productCode},
        success: function (result) {
            listener(result);
        },
        error: function () {
            listener("");
        }
    });
}

function calcPremiTraditional(up, modeBayar, usiaAnak, masaBayar, masaAsuransi,productCode, listener) {
    $.ajax({
        url: '/Ajax/CalculatePremiTraditional',
        type: 'POST',
        dataType: 'json',
        data: { up: up, type: modeBayar, childAge: usiaAnak, term: masaBayar, period: masaAsuransi, productCode: productCode },
        success: function (result) {
            listener(result);
        },
        error: function () {
            listener("");
        }
    });
}

function getProduct(code, listener) {
    $.ajax({
        url: '/Ajax/GetProduct',
        type: 'POST',
        dataType: 'json',
        data: { code: code },
        success: function (result) {
            listener(result);
        },
        error: function () {
            listener("");
        }
    });
}

function getProductCode(listener) {
    $.ajax({
        url: '/Ajax/GetProductCode',
        type: 'POST',
        dataType: 'json',
        success: function (result) {
            listener(result);
        },
        error: function () {
            listener("");
        }
    });
}

function calcTraditionalCOR(up, code, riderType, type, riderCov, category, listener) {
    $.ajax({
        url: '/Ajax/CalculateTraditionalCOR',
        type: 'POST',
        dataType: 'json',
        data: { up: up, code: code, riderType: riderType, type: type, riderCov: riderCov, category: category },
        beforeSend: function () {
            $("body").prepend("<div class=\"overlay\"></div>");
            $(".overlay").css({
                "position": "absolute",
                "width": $(document).width(),
                "height": $(document).height(),
                "z-index": 99999
            }).fadeTo(0, 0.8);
        },
        success: function (result) {
            $(".overlay").remove();
            listener(result);
        },
        error: function () {
            $(".overlay").remove();
            listener("");
        }
    });
}

function getMinAmountByPaymentMethod(product, pm, type, listener) {
    $.ajax({
        url: '/Ajax/GetMinAmountByPaymentMethod',
        type: 'POST',
        dataType: 'json',
        data: { product: product, pm: pm, type: type },
        success: function (result) {
            listener(result);
        },
        error: function () {
            listener("");
        }
    });
}

function getPaymentMethod(code, listener) {
    $.ajax({
        url: '/Ajax/GetPaymentMethod',
        type: 'POST',
        dataType: 'json',
        data: { code: code },
        success: function (result) {
            listener(result);
        },
        error: function () {
            listener("");
        }
    });
}

function resolveReferences(json) {
    if (typeof json === 'string')
        json = JSON.parse(json);

    var byid = {}, // all objects by id
        refs = []; // references to objects that could not be resolved
    json = (function recurse(obj, prop, parent) {
        if (typeof obj !== 'object' || !obj) // a primitive value
            return obj;
        if ("$ref" in obj) { // a reference
            var ref = obj.$ref;
            if (ref in byid)
                return byid[ref];
            // else we have to make it lazy:
            refs.push([parent, prop, ref]);
            return;
        } else if ("$id" in obj) {
            var id = obj.$id;
            delete obj.$id;
            if ("$values" in obj) // an array
                obj = obj.$values.map(recurse);
            else // a plain object
                for (var prop in obj)
                    obj[prop] = recurse(obj[prop], prop, obj)
            byid[id] = obj;
        }
        return obj;
    })(json); // run it!

    for (var i = 0; i < refs.length; i++) { // resolve previously unknown references
        var ref = refs[i];
        ref[0][ref[1]] = byid[refs[2]];
        // Notice that this throws if you put in a reference at top-level
    }
    return json;
}

function calcUnitLinkCOR(up, riderCode, riderType, age, riskClass, riderCategory, unit, listener) {
    $.ajax({
        url: '/Ajax/CalculateUnitLinkCOR',
        type: 'POST',
        dataType: 'json',
        data: { up: up, riderCode: riderCode, riderType: riderType, age: age, riskClass: riskClass, riderCategory: riderCategory, unit: unit },
        beforeSend: function () {
            $("body").prepend("<div class=\"overlay\"></div>");
            $(".overlay").css({
                "position": "absolute",
                "width": $(document).width(),
                "height": $(document).height(),
                "z-index": 99999
            }).fadeTo(0, 0.8);
        },
        success: function (result) {
            $(".overlay").remove();
            listener(result);
        },
        error: function () {
            $(".overlay").remove();
            listener("");
        }
    });
}

function getRiderSumInsured(age, listener) {
    $.ajax({
        url: '/Ajax/GetRiderSumInsured',
        type: 'POST',
        dataType: 'json',
        data: { age: age },
        success: function (result) {
            listener(result);
        },
        error: function () {
            listener("");
        }
    });
}

function getUnitMapping(riderCode, riderType, up, listener) {
    $.ajax({
        url: '/Ajax/GetUnitMapping',
        type: 'POST',
        dataType: 'json',
        data: { riderCode: riderCode, riderType: riderType, up : up },
        success: function (result) {
            listener(result);
        },
        error: function () {
            listener("");
        }
    });
}

function getMaxUnitMapping(riderCode, riderType, up, listener) {
    $.ajax({
        url: '/Ajax/GetMaxUnitMapping',
        type: 'POST',
        dataType: 'json',
        data: { riderCode: riderCode, riderType: riderType, up: up },
        success: function (result) {
            listener(result);
        },
        error: function () {
            listener("");
        }
    });
}

function getSantunanHarian(riderCode, riderType, listener) {
    $.ajax({
        url: '/Ajax/GetSantunanHarian',
        type: 'POST',
        dataType: 'json',
        data: { riderCode: riderCode, riderType: riderType },
        success: function (result) {
            listener(result);
        },
        error: function () {
            listener("");
        }
    });
}

function calculateCOI(up, listener) {
    $.ajax({
        url: '/Ajax/CalculateCOI',
        type: 'POST',
        dataType: 'json',
        data: { up: up },
        success: function (result) {
            listener(result);
        },
        error: function () {
            listener("");
        }
    });
}

function isCoveredByRider(riderCode, age, listener) {
    $.ajax({
        url: '/Ajax/IsCoveredByRider',
        type: 'POST',
        dataType: 'json',
        data: {
            RiderCode: riderCode,
            age: age
        },
        success: function (result) {
            listener(result);
        },
        error: function () {
            listener("");
        }
    });
}
function getSAMultiplier(productCode, age, gender, listener)
{
    $.ajax({
        url: '/Ajax/GetSAMultiplier',
        type: 'POST',
        dataType: 'json',
        data: {
            productCode: productCode,
            age: age,
            type : gender
        },
        success: function (result) {
            listener(result);
        },
        error: function () {
            listener("");
        }
    });
}

function produkChange(produk) {
    var isLastActive = $('#progressbar li:last').hasClass('active');
    if (produk != "HLEDU" && $('#progressbar li').length > 3)
        $('#progressbar li:last').remove();
    else if (produk == "HLEDU" && $('#progressbar li').length <= 3)
        $('#progressbar').append("<li></li>");

    if (isLastActive)
        $('#progressbar li:last').addClass('active');
}
